IF OBJECT_ID (N'dbo.Tiin_Api$ReformatChuThich', N'FN') IS NOT NULL  
    DROP FUNCTION Tiin_Api$ReformatChuThich;  
GO  
CREATE FUNCTION dbo.Tiin_Api$ReformatChuThich(@Content NVARCHAR(max))  
RETURNS NVARCHAR(max)   
AS   
-- Returns the stock level for the product.  
BEGIN  
    DECLARE @ReturnContent NVARCHAR(MAX)
	DECLARE @Replcement NVARCHAR(MAX)
	SET @Replcement = N'style="background: #f5f5f5;font-size:1em; font-style: italic;color: #666;text-align: center;margin: -1em -0.5em 0;padding: 0.25em 0.5em;"';
	SELECT @ReturnContent = REPLACE(@Content,'style="background: none repeat scroll 0 0 #f5f5f5;font-size:13px; color: #666;padding:3px 10px 3px 15px;position:relative;left:-10px;width:105%"',@Replcement);
	SELECT @ReturnContent = REPLACE(@Content,'style="background: none repeat scroll 0 0 #f5f5f5;font-size:13px; font-style: italic;color: #666;padding:3px 10px 3px 15px;position:relative;left:-10px;width:105%"',@Replcement);
	RETURN @ReturnContent
END; 


--Tiin_API$ArticleDetail 799199
ALTER PROCEDURE [dbo].[Tiin_API$ArticleDetail]

	@id INT
AS 
BEGIN
	-- khi id tin bai bang 0 tìm theo slug
	Declare @thematic_id int , @pid int, @cid INT
		
			select top(1) @pid = pid, @cid = cid, @thematic_id = thematic_id from Mess_Cate where mid = @id

			-- Tin bai
			SELECT   id,Title,Lead,Content, Author, dbo.GetSourceName(SID) AS SourceName, slug ,LeadImage, Hit, @pid as pid, @cid as cid,dbo.GetCategoryAlias(@pid) as CategoryAlias,dbo.GetCategoryName(@pid) as CategoryName,dbo.GetIcon(id) as icon,  dbo.UNIX_TIMESTAMP(DatePub) as datePub   from Messages    where ID = @id and Status != 0
			-- Tin khac		
			SELECT TOP(6) mid as id , cid, pid, title, Lead, dbo.GetCategoryAlias(pid) as CategoryAlias, hit,dbo.GetCategoryName(pid) as CategoryName, dbo.GetMedia(mid, 3) AS LeadImage,dbo.GetIcon(mid) as icon,slug, dbo.UNIX_TIMESTAMP(DatePub) as datePub  FROM Mess_Cate_Temp WHERE status = 2 and mid != @id  AND pid = @pid  and Mess_Cate_Temp.datepub < getdate()  ORDER BY Mess_Cate_Temp.DatePub DESC				
			
		
END

GO



EXEC [dbo].[Tiin_API$ArticleDetail] @id= 1139480-- int







