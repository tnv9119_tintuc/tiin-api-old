ALTER PROCEDURE [dbo].[Tiin_API$Cate]
		@category_id int,
		@number      INT,
		@pageIndex   INT = 1
AS
	BEGIN

		DECLARE @total INT, @pageNum INT, @pmax INT, @pmin INT, @pageSize INT, @TID INT, @HID INT, @Num INT
		SET @pageSize = @number

		IF (@number < 5)
			SET @number = 20

		if (@pageIndex > 50)
			set @pageIndex = 50


		-- Danh sach tin bai
		DECLARE @Tinbai TABLE
		(
			rownumber INT,
		--ID INT IDENTITY(1,1) NOT NULL,
			mid       INT      NULL,
			cid       INT      NULL,
			pid       INT      NULL,
			DatePub   DATETIME NULL,
			Title     nvarchar(300),
			slug      nvarchar(300),
			lead      nvarchar(600),
			isSlide   int,
			hit       int
		)

		-- Hoán đổi Category và SubCaegory nếu tham số truyền vào là thư mục con
		DECLARE @IsSubCate BIT
		SET @IsSubCate = 0;

		SELECT @IsSubCate = CASE WHEN c.PID <> 0 THEN 1 ELSE 0 END FROM dbo.Categories c WHERE c.ID = @category_id


		if (@category_id <> 0)
			BEGIN

				-- GET ONE RECORD FROM STATUS IS TOP CATEGORY
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 1, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 5
								AND (pid = @category_id AND @IsSubCate = 0)
					 OR (cid = @category_id AND @IsSubCate = 1)
				ORDER BY [order] asc


				SET @Num = @pageSize * @pageIndex - (SELECT COUNT(*) FROM @Tinbai)
				SET ROWCOUNT @Num

				-- GET CATEGORY 'S LIKE NORMAL
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT ROW_NUMBER() OVER (ORDER BY datepub DESC) + (SELECT COUNT(*) FROM @Tinbai) AS rownumber,
							 mid,
							 cid,
							 pid,
							 datepub,
							 Title,
							 lead,
							 slug,
							 isSlide,
							 hit
				FROM Mess_Cate_Temp
				WHERE 1 = 1
								AND (pid = @category_id AND @IsSubCate = 0)
					 OR (cid = @category_id AND @IsSubCate = 1)
								AND [Status] = 2 AND DatePub < GETDATE()
								AND mid not in (select top (1) id1
																from [Status]
																where [type] = 5
																	AND mid NOT IN (SELECT mid from @Tinbai)
																ORDER BY [order] asc)
				SET ROWCOUNT 0

				SELECT TOP (@pageSize) mid             as id,
									 rownumber,
									 slug,
									 dbo.GetCategoryAlias(pid)   as CategoryAlias,
									 dbo.GetCategoryName(pid)    as CategoryName,
									 dbo.GetCategoryAlias(cid)   as ChildCategoryAlias,
									 dbo.GetCategoryName(cid)    as ChildCategoryName,
									 cid,
									 pid,
									 lead,
									 hit,
									 title,
									 dbo.GetMedia(mid, 1)        AS LeadImage,
									 dbo.UNIX_TIMESTAMP(DatePub) as DatePub,
									 dbo.GetIcon(mid)            as icon,
									 isSlide
				from @Tinbai
				where rownumber > (@pageIndex - 1) * @pageSize
				order by rownumber asc
			end
		else
			-- category = 0 (HomePage)
			BEGIN

				-- Top giữa
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 1, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 1
					and [Status].datepub < GETDATE()
				ORDER BY [order] asc
				-- Top 1
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 2, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 29
					and [Status].datepub < GETDATE()
				ORDER BY [order] asc
				-- Top 2
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 3, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 4
					and [Status].datepub < GETDATE()
				ORDER BY [order] ASC
				-- Top 3
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 4, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 33
					and [Status].datepub < GETDATE()
				ORDER BY [order] ASC
				-- Top 4
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT top (1) 5, id1, cid, pid, datepub, Title, lead, slug, isSlide, 1
				from [Status]
				where [type] = 34
					and [Status].datepub < GETDATE()
				ORDER BY [order] asc

				-- GET 8 Posts in [Top tiêu điểm]
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT ROW_NUMBER() OVER (ORDER BY TBl.datepub DESC) + (SELECT COUNT(*) FROM @Tinbai) AS rownumber, TBL.*
				FROM (SELECT TOP (8) id1, cid, pid, datepub, Title, lead, slug, isSlide, 1 AS hit
							from [Status]
							where [type] = 24
								and [Status].datepub < GETDATE()
								AND id1 NOT IN (SELECT mid FROM @Tinbai)
							ORDER BY [order] ASC) AS TBL


				SET @Num = @pageSize * @pageIndex - (SELECT COUNT(*) FROM @Tinbai)
				SET ROWCOUNT @Num

				-- GET New Posts 
				INSERT INTO @Tinbai (rownumber, mid, cid, pid, DatePub, Title, lead, slug, isSlide, hit)
				SELECT ROW_NUMBER() OVER (ORDER BY datepub DESC) + (SELECT COUNT(*) FROM @Tinbai)  AS rownumber,
							 mid,
							 cid,
							 pid,
							 datepub,
							 Title,
							 lead,
							 slug,
							 isSlide,
							 hit
				FROM Mess_Cate_Temp
				WHERE [Status] = 2
					AND DatePub < GETDATE()
					and mid not in (select mid from @Tinbai)
				SET ROWCOUNT 0
				SELECT TOP (@pageSize) mid             as id,
									 rownumber,
									 slug,
									 dbo.GetCategoryAlias(pid)   as CategoryAlias,
									 dbo.GetCategoryName(pid)    as CategoryName,
									 dbo.GetCategoryAlias(cid)   as ChildCategoryAlias,
									 dbo.GetCategoryName(cid)    as ChildCategoryName,
									 cid,
									 pid,
									 lead,
									 hit,
									 title,
									 dbo.GetMedia(mid, 1)        AS LeadImage,
									 dbo.UNIX_TIMESTAMP(DatePub) as DatePub,
									 dbo.GetIcon(mid)            as icon,
									 isSlide
				from @Tinbai
				where rownumber > (@pageIndex - 1) * @pageSize
				order by rownumber asc

			end


	END
GO
EXEC dbo.[Tiin_API$Cate] @category_id = 10, -- int
												 @number = 20, -- int
												 @pageIndex = 1    -- int
GO
EXEC dbo.[Tiin_API$Cate] @category_id = 10, -- int
												 @number = 20, -- int
												 @pageIndex = 2    -- int