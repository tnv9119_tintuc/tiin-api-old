using System;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;

namespace ViettelMedia.Tests
{
    public class TestFastRead : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            var content = @"
<div class=""more-detail bg_cms_vang"">
    <p><strong>>> Xem thêm: </strong><strong><a href=""http://tiin.vn/chuyen-muc/dep/hoi-ban-than-chuan-con-nha-nguoi-ta-cua-showbiz-viet-khong-khoe-thi-thoi-ma-cu-tha-dang-la-ngat-ngay.html"" title=""Hội bạn thân chuẩn 'con nhà người ta' của showbiz Việt: Không khoe thì thôi mà cứ 'thả dáng' là ngất ngây"">Hội bạn thân chuẩn '<em>con nhà người ta</em>' của showbiz Việt: Không khoe thì thôi mà cứ 'thả dáng' là ngất ngây</a></strong></p>
</div>
<p>Nội dung chú thích ảnh</p>
<div style=""background: #fce5ee;padding:5px 10px 10px"">
    <p>PR cuối bài</p>
</div>
<p> Để xem trực tiếp toàn bộ trận thi đấu cũng như clip bên lề của Đấu Trường Danh Vọng mùa đông 2019, khán giả có thể theo dõi kênh ĐTDV mùa đông 2019 trên Mocha tại <a href=""http://video.mocha.com.vn/DTDV-cn115626.html"">http://video.mocha.com.vn/DTDV-cn115626.html</a>. Hoặc cài app Mocha tại <a href=""http://mocha.com.vn/app"">http://mocha.com.vn/app</a> để xem trực tiếp miễn phí data 3G/4G Viettel.</p>
<p> </p>
<div class=""box-tamsu"" style=""background-color: #f2f0e7;border: 1px solid #b1afae;border-radius: 20px;margin: 0 auto 15px;overflow: hidden;width: 300px;"">
    <div style=""display: block; height: 15px; line-height: 0; overflow: hidden; width: 280px; font-size: 0px; text-align: justify;""> </div>
    <div style=""clear: both; display: block; line-height: 21px; padding: 0px 15px; text-align: justify;"">Từ ngày 22.3 - 26.3,<a href=""http://mocha.com.vn/app""><strong> Mocha </strong></a> phát sóng trực tiếp các trận đấu U23 Việt Nam. Đặc biệt, thuê bao HSSV và Mocha Vip được miễn cước 3G/4G Viettel tốc độ cao.</div>
    <div style=""display: block; font-size: 0px; height: 15px; line-height: 0; overflow: hidden; width: 270px; text-align: justify;""> </div>
</div>
<p><img alt=""Nội dung chú thích ảnh"" src=""../images/default_img.png"" style=""opacity: 1;""></p>
<p class=""p-chuthich"" style=""background: none repeat scroll 0 0 #f5f5f5;font-size:13px; color: #666;padding:3px 10px 3px 15px;position:relative;left:-10px;width:105%"">Nội dung chú thích ảnh</p>
<p> </p>
<div class=""multi-item multi-item-right"" style=""overflow:hidden;"">
    <p class=""multi-item-img"" style=""width:50%;float:right;background:#f1f1f1;margin-left:20px;""><img alt="""" src=""../images/layer-42-1485340922272.jpg"" style=""width:100%;""></p>
    <div class=""multi-content"" style=""background:#f1f1f1"">
        <p>Multi Item Right</p>
    </div>
</div>
<div class=""multi-item multi-item-left"" style=""overflow:hidden;"">
    <p class=""multi-item-img"" style=""width:50%;float:left;background:#f1f1f1;margin-right:20px;""><img alt="""" src=""../images/layer-42-1485340922272.jpg"" style=""width:100%;""></p>
    <div class=""multi-content"" style=""background:#f1f1f1"">
        <p>Multi Item Left</p>
    </div>
</div>
<div class=""multi-item"" style=""overflow:hidden;"">
    <p class=""multi-item-img"" style="";background:#f1f1f1;""><img alt="""" src=""../images/layer-41-1485340826860.jpg"" style=""width:100%;""></p>
    <div class=""multi-content"" style=""background:#f1f1f1"">
        <p>Multi-Item-Center</p>
    </div>
</div>
<div class=""multi-item"" style=""overflow:hidden;"">
    <div class=""multi-content"" style=""background:#f1f1f1"">
        <p class=""multi-item-img"" style=""float:left;margin-right:15px;width:auto;""><img alt=""Test Template 4"" src=""../images/demo.jpg"" style=""width:100%;""></p>
        <p>MultiItem-iCOn</p>
    </div>
</div>
<div class=""multi-item multi-item-graphic"" style=""overflow:hidden;"">
    <p class=""multi-item-img"" style="";background:#f1f1f1;""><img alt="""" src=""../images/layer-41-1485340826860.jpg"" style=""width:100%;""></p>
    <p class=""multi-item-img multi-wap"" style="";background:#f1f1f1;""><img alt="""" src=""../images/layer-41-1485340826860.jpg"" style=""width:100%;""></p>
    <div class=""multi-content"" style=""background:#f1f1f1"">
        <p>Finfo</p>
    </div>
</div>
<div class=""box-tamsu"" style=""background-color: #f2f0e7;border: 1px solid #b1afae;border-radius: 20px;margin: 0 auto 15px;overflow: hidden;width: 280px;"">
    <div style=""display: block;height: 22px;line-height: 0;overflow: hidden;width: 280px; font-size: 0;""> </div>
    <div style="" clear: both;display: block;line-height: 21px;padding: 0 14px;text-align: justify;width: 260px;"">Mọi sự giúp đỡ xin gửi về:</div>
    <div style="" clear: both;display: block;line-height: 21px;padding: 0 14px;text-align: justify;width: 260px;"">Chị <strong>Đinh Thị Dương </strong>(thôn 1, xã Đoàn Kết, huyện Đạ Huoai, tỉnh Lâm Đồng, ĐT: 0979 178 428) </div>
    <div style="" display: block;font-size: 0;height: 21px;line-height: 0;overflow: hidden;width: 270px;""> </div>
</div>
<div class=""box-tamsu"" style=""background-color: #f2f0e7;border: 1px solid #b1afae;border-radius: 20px;margin: 0 auto 15px;overflow: hidden;width: 280px;"">
    <div style=""display: block;height: 22px;line-height: 0;overflow: hidden;width: 280px; font-size: 0;""> </div>
    <div style="" clear: both;display: block;line-height: 21px;padding: 0 14px;text-align: justify;width: 260px;"">Kính mời quý độc giả gửi bài, ảnh hoặc clip tham gia cuộc thi <strong>Ấn tượng tình yêu</strong> theo địa chỉ email <a href=""mailto:Antuongtinhyeu@gmail.com"">Antuongtinhyeu@gmail.com</a> để có cơ hội nhận giải tuần 500 nghìn đồng và giải chung cuộc 5 triệu đồng. Xem thể lệ chi tiết của cuộc thi <a href=""http://tiin.vn/chuyen-muc/yeu/the-le-cuoc-thi-an-tuong-tinh-yeu-de-yeu-thuong-len-tieng.html""><strong>TẠI ĐÂY</strong></a>.</div>
    <div style="" display: block;font-size: 0;height: 21px;line-height: 0;overflow: hidden;width: 270px;""> </div>
</div>
<div class=""tarot-card"">
    <div class=""tarot-card-row"">
        <section class=""singlecard"">
            <div class=""card"">
                <figure class=""front""><img alt="""" src=""../images/tarot.png""></figure>
                <figure class=""back""><img alt="""" src=""../images/tarot_01.png""></figure>
                <div class=""info-card"">
                    <h3>Tình yêu</h3>Có những lúc cần một người ở bên, bạn cũng được mà yêu cũng được...<br>Ước có một người cùng thức mỗi đêm, chia sẻ nỗi niềm để thôi không buồn chán, ước có một người cùng ăn bữa sáng, để thấy thì ra cũng đáng đợi từng ngày
                </div>
            </div>
            <h3 class=""card-h3""> </h3>
        </section>
        <section class=""singlecard"">
            <div class=""card"">
                <figure class=""front""><img alt="""" src=""../images/tarot.png""></figure>
                <figure class=""back""><img alt="""" src=""../images/tarot_02.png""></figure>
                <div class=""info-card"">
                    <h3>Thiên đạo</h3>Có những lúc cần một người ở bên, bạn cũng được mà yêu cũng được...<br>Ước có một người cùng thức mỗi đêm, chia sẻ nỗi niềm để thôi không buồn chán, ước có một người cùng ăn bữa sáng, để thấy thì ra cũng đáng đợi từng ngày
                </div>
            </div>
            <h3 class=""card-h3""> </h3>
        </section>
        <section class=""singlecard"">
            <div class=""card"">
                <figure class=""front""><img alt="""" src=""../images/tarot.png""></figure>
                <figure class=""back""><img alt="""" src=""../images/tarot_03.png""></figure>
                <div class=""info-card"">
                    <h3>Đường đời</h3>Có những lúc cần một người ở bên, bạn cũng được mà yêu cũng được...<br>Ước có một người cùng thức mỗi đêm, chia sẻ nỗi niềm để thôi không buồn chán, ước có một người cùng ăn bữa sáng, để thấy thì ra cũng đáng đợi từng ngày
                </div>
            </div>
            <h3 class=""card-h3""> </h3>
        </section>
        <div class=""info-tarot""> </div>
    </div>
</div>
<p> </p>
";
            if (Request.Form["content"] != null)
            {
                content = Request.Form["content"];
            }

            int total = 0;
            var output = new FastRead().ExtractContentBlocks(content, new Random(100).Next(1, 10000).ToString(), out total);
            Response.ContentType = "application/json";
            Response.Write(JsonConvert.SerializeObject(output,Formatting.Indented));
            Response.End();
        }
    }
}