using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public class Siblings : Page
    {
        protected string page = "1";
        protected string num = "10";
        protected string ArticleId = "0";
        protected string Phone = "";
        protected string Msisdn = "";
        protected string UrlMediaOld = ConfigurationManager.AppSettings["MediaRoot"];
        protected string UrlMediaNew = ConfigurationManager.AppSettings["MediaRootNew"];

        protected void Page_Load(object sender, EventArgs e)
        {
            ArticleId = Common.GetValue("id", "1");
            page = Common.GetValue("page", "1");
            num = Common.GetValue("num", "10");


//            DataArticle dataArticle = null;
            var jsonData = "";
            try
            {
                jsonData = KPILogger.LogKPI<string>("Siblings", () =>
                {
                    string cacheName = "TiinAPI:GetArticleSiblingsSingle:" + ArticleId + ":" + page + ":" + num;
                    return CacheManager.Remember<string>(cacheName,
                        DateTime.Now.AddMinutes(Common.MinutesCache),
                        delegate
                        {
                            var listArticles = new List<MochaArticle>();

                            DataSet ds = Common.GetArticleSiblings(ArticleId, page, num);


                            if (ds.Tables.Count > 0)
                            {
                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    var article = new MochaArticle();
                                    article.Title = row["Title"].ToString();
                                    article.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                                    article.Sapo = row["lead"].ToString();
                                    article.Pid = Convert.ToInt32(row["pid"].ToString());
                                    article.Cid = Convert.ToInt32(row["cid"].ToString());
                                    article.Content = "";
                                    article.AuthorName =
                                        Common.GetValueOrDefault(row["Author"].ToString(), Common.DefaultAuthorName);
                                    article.SourceName =
                                        Common.GetValueOrDefault(row["SourceName"].ToString(),
                                            Common.DefaultSourceName);
                                    article.ParentCategory = row["CategoryName"].ToString().Trim();
                                    article.Category = row["CategoryName"].ToString().Trim();
                                    article.Id = Convert.ToInt32(row["ID"].ToString());
                                    if (article.Id > 884864)
                                    {
                                        article.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                        Path.AltDirectorySeparatorChar +
                                                        row["LeadImage"].ToString();

                                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                                        {
                                            article.Image169 =
                                                ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar +
                                                row["LeadImage420"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        article.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                        Path.AltDirectorySeparatorChar +
                                                        row["LeadImage"].ToString();
                                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                                        {
                                            article.Image169 =
                                                ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar +
                                                row["LeadImage420"].ToString();
                                        }
                                    }

                                    article.Like = 0;
                                    article.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                                    article.Position = 0;
                                    article.Reads = Convert.ToInt32(row["hit"].ToString());
                                    article.Type = 1;
                                    article.StarId = 0;
                                    article.Facebook = "";
                                    article.ThematicName = "";
                                    article.DatePub = Convert.ToInt32(row["datePub"].ToString());
                                    article.Url = Rewrite.GenRewriteUrlDetail(row);
                                    article.Blocks = new FastRead().GetContentBlocks(
                                            Common.GenLinkImageFull(row["Content"].ToString()),
                                            row["id"].ToString(), out var totalBlocks, 3)
                                        .ConvertAll(MochaArticleBlock.Converter);

                                    article.TotalBlocks = totalBlocks;
                                    listArticles.Add(article);
                                }
                            }

                            return JsonConvert.SerializeObject(new DataArticle(listArticles),
                                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
                        });
                });
            }
            catch (Exception exception)
            {
                jsonData = JsonConvert.SerializeObject(new DataArticle(null) {exception = exception},
                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
            }

            Response.JsonString(jsonData);
        }
    }
}