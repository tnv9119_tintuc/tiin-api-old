﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Mocha.Models.Response;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;
using ViettelMedia.TiinApi.WS.News;

namespace ViettelMedia.TiinApi.Mocha
{
    public partial class HotTopic : System.Web.UI.Page
    {
        protected string phone = "";
        protected string MSISDN = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            var page = Common.GetValue("page", "1");
            var num = Common.GetValue("num", "10");
            var thematicsLogged = "";

            try
            {
                thematicsLogged = KPILogger.LogKPI("HotTopic", () =>
                {
                    IEnumerable<object> thematics = new List<object>();
                    var cacheName =
                        String.Join("_", new[] {"Tiin_API$Mocha$HotTopicSingle", page.ToString(), num.ToString()});

                    return CacheManager.Remember(cacheName,
                        DateTime.Now.AddSeconds(Common.SecondsCache),
                        delegate
                        {
                            var ds = Common.Tiin_API_Mocha_HotTopic(page, num);


                            if (ds.Tables.Count > 0)
                            {
                                var tbl = ds.Tables[0].AsEnumerable();
                                thematics = tbl.Select(r => new
                                {
                                    ThematicId = r.Field<int>("thematic_id"),
                                    ThematicName = r.Field<string>("thematic_name"),
                                    ThematicSlug = r.Field<string>("thematic_slug"),
                                    ThematicThumbPath = r.Field<string>("thematic_thumb_path"),
                                }).Distinct().ToList().Select(r => new
                                {
                                    r.ThematicId,
                                    r.ThematicName,
                                    r.ThematicSlug,
                                    r.ThematicThumbPath,
                                    data = tbl.Where(p => p.Field<int>("thematic_id") == r.ThematicId)
                                        .Select(p => DataRowToArticle(p))
                                }).ToList();
                            }

                            return JsonConvert.SerializeObject(new MochaHomeResponse(thematics),
                                new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
                        });
                });
            }
            catch (Exception exception)
            {
                thematicsLogged = JsonConvert.SerializeObject(new MochaHomeResponse(null) {exception = exception},
                    new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
            }

            Response.JsonString(thematicsLogged);
        }


        protected object DataRowToArticle(DataRow row)
        {
            var item = new MochaArticle();

            item.Title = row["Title"].ToString();
            item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
            item.Sapo = row["lead"].ToString();
            item.Pid = Convert.ToInt32(row["pid"].ToString());
            item.Cid = Convert.ToInt32(row["cid"].ToString());
            item.Content = "";
            item.AuthorName = "";
            item.ParentCategory = row.StringValue("CategoryName");
            item.ParentCategoryAlias = row.StringValue("CategoryAlias");
            item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                ? row.StringValue("ChildCategoryName")
                : row.StringValue("CategoryName");
            item.CategoryAlias =
                !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                    ? row.StringValue("ChildCategoryAlias")
                    : row.StringValue("CategoryAlias");
            item.Header = "";
            item.Id = Convert.ToInt32(row["ID"].ToString());
            item.Id = Convert.ToInt32(row["ID"].ToString());
            if (item.Id > 884864)
            {
                item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                             Path.AltDirectorySeparatorChar +
                             row["LeadImage"].ToString();

                if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                {
                    item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                    Path.AltDirectorySeparatorChar +
                                    row["LeadImage420"].ToString();
                }
            }
            else
            {
                item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                             Path.AltDirectorySeparatorChar +
                             row["LeadImage"].ToString();
                if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                {
                    item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                    Path.AltDirectorySeparatorChar +
                                    row["LeadImage420"].ToString();
                }
            }

            item.Like = 0;
            item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
            item.Position = 0;
            item.Reads = Convert.ToInt32(row["hit"].ToString());
            item.Type = 1;
            item.StarId = 0;
            item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
            item.Url = Rewrite.GenRewriteUrlDetail(row);

            item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                    row["id"].ToString(), out var totalBlocks, 3)
                .ConvertAll(MochaArticleBlock.Converter);

            item.TotalBlocks = totalBlocks;
            return item;
        }


        protected List<MochaArticle> DataTableToListArticle(DataTable dataTable, string Header = "")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();

                    item.Title = row["Title"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> DataTableToListThematic(DataTable dataTable, string Header = "Ngay lúc này")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.LatestTitle = row["Title"].ToString();
                    item.Title = row["thematic_name"].ToString();
                    item.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["thematic_id"].ToString());
                    var postId = Convert.ToInt32(row["Id"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
//                throw;
                return null;
            }

            return articles;
        }

        protected List<MochaArticle> dataTableToListQuotes(DataTable dataTable)
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();
                    item.Quote = row["Quote"].ToString();
                    item.Poster = row["Poster"].ToString();
                    item.Title = row["Title"].ToString();
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = "Câu trích dẫn";
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();

                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                        {
                            item.Image169 = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar +
                                            row["LeadImage420"].ToString();
                        }
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
//                throw;
                return null;
            }

            return articles;
        }
    }
}