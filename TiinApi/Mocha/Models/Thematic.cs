using System.Collections;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ViettelMedia.TiinApi.Models;

namespace ViettelMedia.TiinApi.Mocha.Models
{
    public class Thematic
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string CategoryAlias { get; set; }
        public string CategoryName { get; set; }
        public string Image { get; set; }
    }

    public class Quote
    {
        [JsonProperty("ID")]
        public int Id { get; set; }
        public string Title { get; set; }
        public string Desc { get; set; }
        public string Link { get; set; }
    }

    public class Cate
    {
        public int CategoryID { get; set; }
        public string CategoryName { get; set; }
        public string Color { get; set; }
        public int TypeDisplay { get; set; }
        public string Url { get; set; }
        public IList data { get; set; }
    }
}