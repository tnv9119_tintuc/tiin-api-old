using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.ServiceModel.Channels;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Models;

namespace ViettelMedia.TiinApi.Mocha.Models
{
    public class MochaArticle
    {
        [JsonProperty("ID")] public int Id { get; set; }

        [JsonProperty("Cid")] public int Cid { get; set; }

        [JsonProperty("Pid")] public int Pid { get; set; }

        [JsonProperty("Url")] public string Url { get; set; }

        [JsonProperty("Title")] public string Title { get; set; }

        [JsonProperty("Slug")] public string Slug { get; set; }


        [JsonProperty("Image")] public string Image { get; set; }

        [JsonProperty("Image169")] public string Image169 { get; set; }

        [JsonProperty("Content")] public string Content { get; set; }

        [JsonProperty("Shapo")] public string Sapo { get; set; }

        [JsonProperty("DatePub")] public int DatePub { get; set; }

        [JsonProperty("Like")] public int Like { get; set; }

        [JsonProperty("Comment")] public int Comment { get; set; }

        [JsonProperty("Reads")] public int Reads { get; set; }

        [JsonProperty("Type")] public int Type { get; set; }

        [JsonProperty("Category")] public string Category { get; set; }

        [JsonProperty("CategoryAlias")] public string CategoryAlias { get; set; }

        [JsonProperty("ParentCategory")] public string ParentCategory { get; set; }

        [JsonProperty("ParentCategoryAlias")] public string ParentCategoryAlias { get; set; }

        [JsonProperty("FanCount")] public int FanCount { get; set; }

        [JsonProperty("StarId")] public int StarId { get; set; }

        [JsonProperty("NameStar")] public string NameStar { get; set; }

        [JsonProperty("AuthorName")] public string AuthorName { get; set; }

        [JsonProperty("SourceName")] public string SourceName { get; set; }

        [JsonProperty("VideoId")] public int VideoId { get; set; }

        [JsonProperty("Position")] public int Position { get; set; }

        [JsonProperty("TypeIcon")] public int LoaiBai { get; set; }

        [JsonProperty("Facebook")] public string Facebook { get; set; }


        [JsonProperty("MediaUrl")] public string MediaUrl { get; set; }

        [JsonProperty("ThematicId")] public int ThematicId { get; set; }
        
        [JsonProperty("ThematicName")] public string ThematicName { get; set; }

        [JsonProperty("Body")] public IList Blocks { get; set; }

        [JsonProperty("BodySize")] public int TotalBlocks { get; set; }


        [JsonProperty("Header")] public string Header { get; set; }
        [JsonProperty("LatestTitle")] public string LatestTitle { get; set; }


        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Quote { get; set; }

        [JsonProperty(NullValueHandling = NullValueHandling.Ignore)]
        public string Poster { get; set; }

        public long unixTime { get; set; }

        public MochaArticle()
        {
            Id = 0;
            Cid = 0;
            Pid = 0;
            Url = "";
            Title = "";
            Slug = "";
            Image = "";
            Image169 = "";
            Content = "";
            Sapo = "";
            DatePub = 0;
            Like = 0;
            Comment = 0;
            Reads = 0;
            Type = 0;
            Category = "";
            CategoryAlias = "";
            ParentCategory = "";
            ParentCategoryAlias = "";
            FanCount = 0;
            StarId = 0;
            NameStar = "";
            AuthorName = "";
            SourceName = "";
            VideoId = 0;
            Position = 0;
            LoaiBai = 0;
            Facebook = "";
            MediaUrl = "";
            ThematicName = "";
            Blocks = new List<MochaArticleBlock>();
            Header = "";
            LatestTitle = "";
        }


        public static Converter<TiinWeb.Solr.Message, MochaArticle> SolrConverter = input =>
        {
            var ma = new MochaArticle
            {
                Id = int.Parse(input.Id),
                Cid = input.Cid,
                Pid = input.Pid,
                Title = input.Title,
                Slug = input.Slug,
                Sapo = input.Lead,
                Image = input.LeadImage,
                DatePub = (int) input.DatePub.Subtract(new DateTime(1970, 1, 1)).TotalSeconds,
                Category = input.CategoryName,
                CategoryAlias = input.CategoryAlias,
                ParentCategory = input.CategoryName,
                ParentCategoryAlias = input.CategoryAlias,
                LoaiBai = input.Icon,
            };

            if (ma.Id > TiinWeb.Solr.Message.MAX_MEDIA_OLD_ID)
            {
                ma.Image = TiinWeb.Solr.Message.MEDIA_ROOT_NEW.TrimEnd('/') +
                           Path.AltDirectorySeparatorChar + ma.Image;
            }
            else
            {
                ma.Image = TiinWeb.Solr.Message.MEDIA_ROOT.TrimEnd('/') +
                           Path.AltDirectorySeparatorChar + ma.Image;
            }

            if (!string.IsNullOrEmpty(ma.Content))
            {
                ma.Blocks = new FastRead().GetContentBlocks(
                        Common.GenLinkImageFull(input.Content),
                        input.Id, out var totalBlocks)
                    .ConvertAll(MochaArticleBlock.Converter);
                ma.TotalBlocks = totalBlocks;
            }

            ma.Url = Rewrite.GenRewriteUrlDetail(ma);


            return ma;
        };
    }
}