using System;
using System.Collections;

namespace ViettelMedia.TiinApi.Mocha.Models.Response
{
    [Serializable]
    public class MochaHomeSection     
    {
        public MochaHomeSection(string header, int position, IEnumerable data)
        {
            Type = "Home";
            Header = header;
            Position = position;
            this.data = data;
        }

        public MochaHomeSection()
        {
            
        }

        public string Type { get; set; }
        public string Header { get; set; }
        public int Position { get; set; }
        public object data { get; set; }
    }
}