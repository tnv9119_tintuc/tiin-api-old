using System;
using System.ServiceModel;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;
using ViettelMedia.TiinApi.Models;

namespace ViettelMedia.TiinApi.Mocha.Models
{
    public class MochaArticleBlock
    {
        public int Type { get; set; }


        public string Content { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public string Media { get; set; }

        public string Poster { get; set; }

        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Template { get; set; }


        public static Converter<ArticleBlock, MochaArticleBlock> Converter = input => new MochaArticleBlock
        {
            Type = input.Type,
            Content = input.Type == 2 ? input.Image : input.Type == 3 ? input.Video : input.Content,
            Height = input.Height, Width = input.Width,
            Media = input.Video,
            Poster = input.Type == 3 ? input.Image : "",
            Template = input.Template
        };
    }
}