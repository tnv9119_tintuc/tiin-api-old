using System;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace tiin.mocha.api.TiinApi.Mocha.Models {
    public class MochaVideo {
        public int Id { get; set; }
        public int Pid { get; set; }
        public int Cid { get; set; }
        public string Title { get; set; }
        public string Shapo { get; set; }
        public string Url { get; set; }
        public string Image { get; set; }
        public string Media { get; set; }
        public string Poster { get; set; }
        public int Sid { get; set; }
        public string SourceName { get; set; }
        public string SourceIcon { get; set; }
        public string DatePub { get; set; }
        public int View { get; set; }
        public long UnixTime { get; set; }

        [JsonProperty(PropertyName = "aspecRatio")]
        public string aspecRatio { get; set; }

        public long duration { get; set; }

        public string inf { get; set; }
    }
}