﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public partial class Category : System.Web.UI.Page
    {
        protected string page = "1";
        protected string num = "10";
        protected string category_id = "0";
        protected string phone = "";
        protected string MSISDN = "";

        protected long unixTime = 0;


        protected void Page_Load(object sender, EventArgs e)
        {
            page = Common.GetValue("page", "1");
            num = Common.GetValue("num", "10");
            category_id = Common.GetValue("id", "0");
            unixTime = long.Parse(Common.GetValue("unixTime", "0"));


            string data = KPILogger.LogKPI("Category", () =>
            {
                var jsonData = "";

                string cacheName =
                    "getArticleOfCategoryHaveContentSingle_" + category_id + "_" + unixTime + "_" + page + "_" + num;
                jsonData = CacheManager.Remember<string>(cacheName,
                    DateTime.Now.AddSeconds(Common.SecondsCache),
                    delegate
                    {
                        var dataArticle = new DataArticle(null);

                        try
                        {
                            var mochaArticles = new List<MochaArticle>();
                            var ds = Common.getArticleOfCategoryHaveContent(category_id, page, num, unixTime);

                            if (ds.Tables.Count > 0)
                            {
                                var unixTime = 0;
                                if (ds.Tables[1].Rows.Count == 1)
                                {
                                    unixTime = Convert.ToInt32(ds.Tables[1].Rows[0]["@unixTime"].ToString());
                                }

                                foreach (DataRow row in ds.Tables[0].Rows)
                                {
                                    var article = new MochaArticle();
                                    article.Title = row["Title"].ToString();
                                    article.Slug = row.HasColumn("slug") ? row["slug"].ToString() : "";
                                    article.Sapo = row["lead"].ToString();
                                    article.Pid = Convert.ToInt32(row["pid"].ToString());
                                    article.Cid = Convert.ToInt32(row["cid"].ToString());
                                    article.Content = "";
                                    article.AuthorName = "";
                                    article.ParentCategory = row.StringValue("CategoryName");
                                    article.ParentCategoryAlias = row.StringValue("CategoryAlias");
                                    article.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                                        ? row.StringValue("ChildCategoryName")
                                        : row.StringValue("CategoryName");
                                    article.CategoryAlias =
                                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                                            ? row.StringValue("ChildCategoryAlias")
                                            : row.StringValue("CategoryAlias");
                                    article.Id = Convert.ToInt32(row["ID"].ToString());
                                    if (article.Id > 884864)
                                    {
                                        article.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                        Path.AltDirectorySeparatorChar +
                                                        row["LeadImage"].ToString();

                                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                                        {
                                            article.Image169 =
                                                ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar +
                                                row["LeadImage420"].ToString();
                                        }
                                    }
                                    else
                                    {
                                        article.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                        Path.AltDirectorySeparatorChar +
                                                        row["LeadImage"].ToString();
                                        if (!row.IsNullOrEmptyStringValue("LeadImage420"))
                                        {
                                            article.Image169 =
                                                ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar +
                                                row["LeadImage420"].ToString();
                                        }
                                    }

                                    article.Like = 0;
                                    article.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                                    article.Position = 0;
                                    article.Reads = Convert.ToInt32(row["hit"].ToString());
                                    article.Type = 1;
                                    article.StarId = 0;
                                    article.Facebook = "";
                                    article.ThematicName = "";
                                    article.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    article.Url = Rewrite.GenRewriteUrlDetail(row);
                                    article.Blocks = new FastRead().GetContentBlocks(
                                            Common.GenLinkImageFull(row["Content"].ToString()),
                                            row["id"].ToString(), out var totalBlocks, 3)
                                        .ConvertAll(MochaArticleBlock.Converter);

                                    article.TotalBlocks = totalBlocks;
                                    article.unixTime = unixTime;
                                    mochaArticles.Add(article);
                                }

                                dataArticle.data = mochaArticles;
                            }
                        }
                        catch (Exception ex)
                        {
                            dataArticle.exception = ex;
                        }

                        return JsonConvert.SerializeObject(dataArticle,
                            new JsonSerializerSettings {NullValueHandling = NullValueHandling.Include});
                    });
                return jsonData;
            });
            Response.JsonString(data);
        }
    }
}