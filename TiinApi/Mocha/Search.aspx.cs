﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using Newtonsoft.Json;
using TiinWeb.Solr;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Mocha.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Mocha
{
    public class Search : System.Web.UI.Page
    {
        protected string keywords = "";
        protected string page = "";
        protected string num = "";
        public string EntityToUnicode(string html) {
            var replacements = new Dictionary<string, string>();
            var regex = new Regex("(&[a-zA-Z]{2,11};)");
            foreach (Match match in regex.Matches(html)) {
                if (!replacements.ContainsKey(match.Value)) { 
                    var unicode = HttpUtility.HtmlDecode(match.Value);
                    if (unicode.Length == 1) {
                        replacements.Add(match.Value, string.Concat("&#", Convert.ToInt32(unicode[0]), ";"));
                    }
                }
            }
            foreach (var replacement in replacements) {
                html = html.Replace(replacement.Key, replacement.Value);
            }
            return html;
        }
        
        private static readonly Regex HtmlEntityRegex = new Regex("&(#)?([a-zA-Z0-9]*);");

        public static string HtmlDecode(string html)
        {
            if (string.IsNullOrEmpty(html)) return html;
            return HtmlEntityRegex.Replace(html, x => x.Groups[1].Value == "#"
                ? ((char)int.Parse(x.Groups[2].Value)).ToString()
                : HttpUtility.HtmlDecode(x.Groups[0].Value));
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            keywords = Request.QueryString["keywords"];
            page = Common.GetValue("page", "1");
            num = Common.GetValue("num", "10");
            

            var dataArticle = KPILogger.LogKPI("Search", () =>
            {
                DataArticle _dataArticle = new DataArticle(null);

                try
                {
                    var pageIndex = int.Parse(page);
                    var pageSize = int.Parse(num);
                    var _solrResponse = new SolrResponse();

                    using (var client = new WebClient())
                    {
                        client.Encoding = Encoding.UTF8;

                        try
                        {
//                            var keyword = WebUtility.HtmlDecode(Server.UrlDecode(keywords));
                            var keyword = Uri.UnescapeDataString(keywords);
                            Response.Write("UnescapeDataString: " + keyword + "\n");

                            keyword = keyword.Replace(' ', '+');

                            Response.Headers.Add("X-KeyWord",keyword);
                            
                            Response.Write("Replace: " + keyword + "\n");

                            var url = ConfigurationManager.AppSettings["Solr_Url"] +
                                      "\"" + keyword + "\"" +
                                      "&start=" + (pageIndex - 1) * pageSize +
                                      "&rows=" + pageSize + "";
//                                      "&hl=on&hl.fl=title";
                            var data = client.DownloadString(url);
                            Response.Headers.Add("X-Url",keyword);

     
                            _solrResponse = JsonConvert.DeserializeObject<SolrResponse>(data);


                            if (_solrResponse.Highlighting != null)
                            {
                                foreach (var doc in _solrResponse.Response.Docs)
                                {
                                    var jObj = _solrResponse.Highlighting.SelectToken(doc.Id + ".title[0]");
                                    if (jObj != null)
                                    {
                                        doc.Title = jObj.ToString();
                                    }
                                }
                            }
                        }
                        catch (Exception exception)
                        {
                            _solrResponse = new SolrResponse
                            {
                                ResponseHeader = new ResponseHeader
                                    {Status = 1, QTime = 0, Params = new ParamsObject {Q = ""}},
                                Response = new Response {NumFound = 0, Docs = new List<Message>(0), Start = 0},
                                Highlighting = null
                            };
                        }
                    }
                    // DataSet ds = Common.searchDetail(keywords, page);

                    // var data = DataTableToListArticle(ds.Tables[0]);

                    _dataArticle.data = _solrResponse.Response.Docs.ConvertAll(MochaArticle.SolrConverter);
                    ;
                }
                catch (Exception ex)
                {
                    _dataArticle.exception = ex;
                }

                return _dataArticle;
            });

            Response.Json(dataArticle);
        }

        public static string LocalRemoveXXS(string strWords)
        {
            //News
            string[] badChars =
            {
                "=", "xp_", ";", "--", "<", ">", "script", "iframe", "delete", "drop", "exec", "insert", "'", "/\\*",
                "\\*/",
                "--", ";--", ";", "@@", "@",
                "char", "nchar", "varchar", "nvarchar",
                "alter", "begin", "cast", "create", "cursor", "declare", "delete", "drop", "end", "exec", "execute",
                "fetch", "insert", "kill", "open",
                "select", "sys", "sysobjects", "syscolumns",
                "table", "update"
            };

            string newChars = strWords;

            foreach (string str in badChars)
            {
                newChars = Regex.Replace(newChars, str, "+", RegexOptions.IgnoreCase);
            }

            newChars = newChars.Replace(' ', '+');

            return newChars;
        }

        protected List<MochaArticle> DataTableToListArticle(DataTable dataTable, string Header = "")
        {
            var articles = new List<MochaArticle>();
            try

            {
                foreach (DataRow row in dataTable.Rows)
                {
                    var item = new MochaArticle();

                    item.Title = row["Title"].ToString();
                    item.Sapo = row["lead"].ToString();
                    item.Pid = Convert.ToInt32(row["pid"].ToString());
                    item.Cid = Convert.ToInt32(row["cid"].ToString());
                    item.Content = "";
                    item.AuthorName = "";
                    item.ParentCategory = row.StringValue("CategoryName");
                    item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                    item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                        ? row.StringValue("ChildCategoryName")
                        : row.StringValue("CategoryName");
                    item.CategoryAlias =
                        !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                            ? row.StringValue("ChildCategoryAlias")
                            : row.StringValue("CategoryAlias");
                    item.Header = Header;
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    item.Id = Convert.ToInt32(row["ID"].ToString());
                    if (item.Id > 884864)
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                    }
                    else
                    {
                        item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                     Path.AltDirectorySeparatorChar +
                                     row["LeadImage"].ToString();
                    }

                    item.Like = 0;
                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                    item.Position = 0;
                    item.Reads = Convert.ToInt32(row["hit"].ToString());
                    item.Type = 1;
                    item.StarId = 0;
                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                    item.Url = Rewrite.GenRewriteUrlDetail(row);
                    item.Header = Header;

                    item.Blocks = new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()),
                            row["id"].ToString(), out var totalBlocks, 3)
                        .ConvertAll(MochaArticleBlock.Converter);

                    item.TotalBlocks = totalBlocks;
                    articles.Add(item);
                }
            }
            catch (Exception e)
            {
//                Console.WriteLine(e);
                throw;
                return null;
            }

            return articles;
        }
    }
}