﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
    public partial class GetProfileOfStar : System.Web.UI.Page
    {
        protected string star_id = "0";
        protected string phone = "";
        protected string MSISDN = "";

        protected void Page_Load(object sender, EventArgs e)
        {
            star_id = Common.GetValue("star_id", "1");

            List<ProfileOfStar> item1 = new List<ProfileOfStar>();

            DataProfileOfStar dataArticle = new DataProfileOfStar(null);

            try
            {
                DataSet ds = Common.getProfileOfStar(star_id);



                if (ds.Tables.Count > 0)
                {

                    foreach (DataRow row in ds.Tables[0].Rows)
                    {
                        ProfileOfStar item = new ProfileOfStar();
                        item.Address = row["address"].ToString();
                        item.Fullname = row["fullname"].ToString();
                        item.FanCount = Convert.ToInt32(row["fan_count"].ToString());
                        item.Gender = Convert.ToInt32(row["gender"].ToString());                     
                        item.Id = Convert.ToInt32(row["ID"].ToString());
                        item.AvatarPath = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') + Path.AltDirectorySeparatorChar + row["avatar_path"].ToString();
                        item.IsSaoFan = Convert.ToInt32(row["is_saofan"].ToString());
                        item.Profile = row["profile"].ToString();
                        item.WeekFanCount = Convert.ToInt32(row["week_fan_count"].ToString());                        
                        item1.Add(item);
                    }

                    dataArticle.data = item1;
                    
                    //Log dữ liệu
                    if ((HttpContext.Current.Session["MSISDN"] != null))
                    {
                        //lay so msisdn tu session
                        MSISDN = Session["MSISDN"].ToString();
                        MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);

                    }
                    else
                    {
                        MSISDN = Common.GetMSISDNOnHeader();
                        MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                        if (MSISDN.Length < 5)
                        {
                            phone = Common.getPhoneNumber(Request);
                            MSISDN = LibCRBT.CheckPhoneValidAll(phone);
                            HttpContext.Current.Session["MSISDN"] = MSISDN;
                        }
                        else
                        {

                            HttpContext.Current.Session["MSISDN"] = MSISDN;
                        }
                    }

                    if (MSISDN.Length > 4)
                    {
                        Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(), Request.RawUrl.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                dataArticle.exception = ex;
            }

            Response.Json(dataArticle);
        }
    }
}
