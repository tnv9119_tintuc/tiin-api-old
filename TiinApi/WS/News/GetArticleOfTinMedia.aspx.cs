﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using System.Web.UI;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetArticleOfTinMedia : Page
      {
            protected string page   = "1";
            protected string num    = "10";
            protected string phone  = "";
            protected string MSISDN = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  page = Common.GetValue("page", "1");
                  num  = Common.GetValue("num",  "10");

                  var _articles = new List<Article>();

                  var _dataArticle = new DataArticle(null);

                  try
                  {
                        DataSet ds = Common.getArticleOfTinMedia(page, num);


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Article item = new Article();
                                    item.Title          = row["Title"].ToString();
                                    item.Sapo           = row["lead"].ToString();
                                    item.Pid            = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid            = Convert.ToInt32(row["cid"].ToString());
                                    item.Content        = "";
                                    item.AuthorName     = "";
                                    item.ParentCategory = row["CategoryName"].ToString();
                                    item.Category       = row["CategoryName"].ToString();
                                    item.Id             = Convert.ToInt32(row["ID"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') + Path.AltDirectorySeparatorChar +
                                                       row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') + Path.AltDirectorySeparatorChar +
                                                       row["LeadImage"].ToString();
                                    }

                                    item.Like         = 0;
                                    item.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    item.Position     = 0;
                                    item.Reads        = Convert.ToInt32(row["hit"].ToString());
                                    item.Type         = 1;
                                    item.StarId       = 0;
                                    item.Facebook     = "";
                                    item.ThematicName = "";
                                    item.DatePub      = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    item.Url          = Rewrite.GenRewriteUrlDetail(row);
                                    _articles.Add(item);
                              }


                              //Log dữ liệu
                              if ((HttpContext.Current.Session["MSISDN"] != null))
                              {
                                    //lay so msisdn tu session
                                    MSISDN = Session["MSISDN"].ToString();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              }
                              else
                              {
                                    MSISDN = Common.GetMSISDNOnHeader();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                                    if (MSISDN.Length < 5)
                                    {
                                          phone                                 = Common.getPhoneNumber(Request);
                                          MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                                    else
                                    {
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                              }

                              if (MSISDN.Length > 4)
                              {
                                    Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(),
                                                  Request.RawUrl.ToString(),
                                                  Request.ServerVariables["REMOTE_ADDR"].ToString());
                              }
                        }
                  }
                  catch (Exception ex)
                  {
                        _dataArticle.exception = ex;
                  }

                  _dataArticle.data = _articles;
                  Response.Json(_dataArticle);
            }
      }
}