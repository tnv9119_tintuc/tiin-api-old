﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetVideoDetail : System.Web.UI.Page
      {
            private string video_id = "0";
            private string phone    = "";
            private string MSISDN   = "";
            private string page     = "1";
            private string num      = "10";

            protected void Page_Load(object sender, EventArgs e)
            {
                  video_id = Common.GetValue("video_id", "0");
                  page     = Common.GetValue("page",     "1");
                  num      = Common.GetValue("num",      "10");

                  List<Article> item1 = new List<Article>();

                  DataArticle dataArticle = new DataArticle(null);

                  try
                  {
                        DataSet ds = Common.getVideoDetail(video_id, page, num);


                        if (ds.Tables.Count > 0)
                        {
                              if (page == "1")
                              {
                                    foreach (DataRow row in ds.Tables[0].Rows)
                                    {
                                          Article item = new Article();
                                          item.Title   = row["Title"].ToString();
                                          item.Sapo    = row["lead"].ToString();
                                          item.Pid     = Convert.ToInt32(row["pid"].ToString());
                                          item.Cid     = Convert.ToInt32(row["cid"].ToString());
                                          item.Content = "";
                                          item.AuthorName =
                                                (string.IsNullOrEmpty(row["Author"].ToString()) ? "Pv" : row["Author"].ToString()) + " - " + row["SourceName"].ToString();
                                          item.SourceName           = row["SourceName"].ToString();
                                          item.ParentCategory       = row.StringValue("CategoryName");
                                          item.ParentCategoryAlias = row.StringValue("CategoryAlias");
                                          item.Category = !row.IsNullOrEmptyStringValue("ChildCategoryName")
                                                                ? row.StringValue("ChildCategoryName")
                                                                : row.StringValue("CategoryName");
                                          item.CategoryAlias =
                                                !row.IsNullOrEmptyStringValue("ChildCategoryAlias")
                                                      ? row.StringValue("ChildCategoryAlias")
                                                      : row.StringValue("CategoryAlias");

                                          item.Id = Convert.ToInt32(row["ID"].ToString());
                                          if (item.Id > 884864)
                                          {
                                                item.Image =
                                                      ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                      Path.AltDirectorySeparatorChar                                +
                                                      row["LeadImage"].ToString();
                                          }
                                          else
                                          {
                                                item.Image =
                                                      ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                      Path.AltDirectorySeparatorChar                             +
                                                      row["LeadImage"].ToString();
                                          }

                                          item.ImageLarge = "";
                                          item.NameStar   = "";


                                          item.Like          = 0;
                                          item.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                          item.Position      = 0;
                                          item.Reads         = Convert.ToInt32(row["hit"].ToString());
                                          item.Type          = 1;
                                          item.StarId       = 0;
                                          item.Facebook      = "";
                                          item.ThematicName = "";

                                          if (item.Id > 884864)
                                          {
                                                item.MediaUrl =
                                                      ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                      Path.AltDirectorySeparatorChar                                +
                                                      row["link"].ToString();
                                          }
                                          else
                                          {
                                                item.MediaUrl =
                                                      ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                      Path.AltDirectorySeparatorChar                             +
                                                      row["link"].ToString();
                                          }

                                          item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                          item.Url     = Rewrite.GenRewriteUrlDetail(row);

                                          item1.Add(item);
                                    }
                              }

                              foreach (DataRow row in ds.Tables[1].Rows)
                              {
                                    Article item = new Article();
                                    item.Title   = row["Title"].ToString();
                                    item.Sapo    = row["lead"].ToString();
                                    item.Pid     = Convert.ToInt32(row["pid"].ToString());
                                    item.Cid     = Convert.ToInt32(row["cid"].ToString());
                                    item.Content = "";
                                    item.AuthorName =
                                          (string.IsNullOrEmpty(row["Author"].ToString()) ? "Pv" : row["Author"].ToString()) + " - " + row["SourceName"].ToString();
                                    item.SourceName           = row["SourceName"].ToString();
                                    item.ParentCategoryAlias = "";
                                    item.CategoryAlias        = "";
                                    item.ParentCategory       = row["CategoryName"].ToString();
                                    item.Category = row["CategoryName"].ToString();
                                    item.Id                    = Convert.ToInt32(row["ID"].ToString());
//                                    item.rownumber             = Convert.ToInt32(row["rownumber"].ToString());
                                    if (item.Id > 884864)
                                    {
                                          item.Image =
                                                ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar                                +
                                                row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          item.Image =
                                                ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar                             +
                                                row["LeadImage"].ToString();
                                    }

                                    item.Like     = 0;
                                    item.LoaiBai = Convert.ToInt32(row["icon"].ToString());
                                    item.Position = 0;
                                    item.Reads    = Convert.ToInt32(row["hit"].ToString());
                                    item.Type     = 1;
                                    item.StarId  = 0;
                                    if (item.Id > 884864)
                                    {
                                          item.MediaUrl =
                                                ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar                                +
                                                row["link"].ToString();
                                    }
                                    else
                                    {
                                          item.MediaUrl =
                                                ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                                Path.AltDirectorySeparatorChar                             +
                                                row["link"].ToString();
                                    }

                                    item.ImageLarge   = "";
                                    item.Facebook      = "";
                                    item.NameStar     = "";
                                    item.ThematicName = "";


                                    item.DatePub = Convert.ToInt32(row["datePub"].ToString()) - 25200;
                                    item.Url     = Rewrite.GenRewriteUrlDetail(row);
                                    item1.Add(item);
                              }


                              dataArticle.data = item1;

                              //Log dữ liệu
                              if ((HttpContext.Current.Session["MSISDN"] != null))
                              {
                                    //lay so msisdn tu session
                                    MSISDN = Session["MSISDN"].ToString();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              }
                              else
                              {
                                    MSISDN = Common.GetMSISDNOnHeader();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                                    if (MSISDN.Length < 5)
                                    {
                                          phone                                 = Common.getPhoneNumber(Request);
                                          MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                                    else
                                    {
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                              }

                              if (MSISDN.Length > 4)
                              {
                                    Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(),
                                                  Request.RawUrl.ToString(),
                                                  Request.ServerVariables["REMOTE_ADDR"].ToString());
                              }
                        }
                  }
                  catch (Exception ex)
                  {
                        dataArticle.exception = ex;
                  }

                  Response.Json(dataArticle);
            }
      }
}