﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using HtmlAgilityPack;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetListArticleOfNewMultimedia : System.Web.UI.Page
      {
            protected string page        = "1";
            protected string num         = "10";
            protected string Phone       = "";
            protected string Msisdn      = "";
            protected string UrlMediaOld = ConfigurationManager.AppSettings["MediaRoot"];
            protected string UrlMediaNew = ConfigurationManager.AppSettings["MediaRootNew"];


        protected void Page_Load(object sender, EventArgs e)
            {
                  page = Common.GetValue("page", "1");
                  num  = Common.GetValue("num",  "10");

                  List<Article> listArticles = new List<Article>();

                  DataArticle dataArticle = null;

                  Response.Clear();
                  Response.ContentType = "application/json; charset=utf-8";

                  try
                  {
                        DataSet ds = Common.GetListArticleOfMessageType(6, page, num);

                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    var article = new Article();
                                    article.Title          = row["Title"].ToString();
                                    article.Sapo           = row["lead"].ToString();
                                    article.Pid            = Convert.ToInt32(row["pid"].ToString());
                                    article.Cid            = Convert.ToInt32(row["cid"].ToString());
                                    article.Content        = "";
                                    article.AuthorName     = Common.GetValueOrDefault(row["Author"].ToString(),     Common.DefaultAuthorName);
                                    article.SourceName     = Common.GetValueOrDefault(row["SourceName"].ToString(), Common.DefaultSourceName);
                                    article.ParentCategory = row["CategoryName"].ToString().Trim();
                                    article.Category       = row["CategoryName"].ToString().Trim();
                                    article.Id             = Convert.ToInt32(row["ID"].ToString());
                                    if (article.Id > 884864)
                                    {
                                          article.Image = ConfigurationManager.AppSettings["MediaRootNew"] + row["LeadImage"].ToString();
                                    }
                                    else
                                    {
                                          article.Image = ConfigurationManager.AppSettings["MediaRoot"] + row["LeadImage"].ToString();
                                    }

                                    article.Like         = 0;
                                    article.LoaiBai      = Convert.ToInt32(row["icon"].ToString());
                                    article.Position     = 0;
                                    article.Reads        = Convert.ToInt32(row["hit"].ToString());
                                    article.Type         = 1;
                                    article.StarId       = 0;
                                    article.Facebook     = "";
                                    article.ThematicName = "";
                                    article.DatePub      = Convert.ToInt32(row["datePub"].ToString());
                                    article.Url          = Rewrite.GenRewriteUrlDetail(row);
                                    article.Blocks =
                                          new FastRead().GetContentBlocks(Common.GenLinkImageFull(row["Content"].ToString()), row["id"].ToString(), out var totalBlocks, 3);
                                    article.TotalBlocks = totalBlocks;
                                    listArticles.Add(article);
                              }
                        }


                        //Log dữ liệu
                        if ((HttpContext.Current.Session["MSISDN"] != null))
                        {
                              //lay so msisdn tu session
                              Msisdn = Session["MSISDN"].ToString();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                        }
                        else
                        {
                              Msisdn = Common.GetMSISDNOnHeader();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                              if (Msisdn.Length < 5)
                              {
                                    Phone                                 = Common.getPhoneNumber(Request);
                                    Msisdn                                = LibCRBT.CheckPhoneValidAll(Phone);
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                              else
                              {
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                        }

                        if (Msisdn.Length > 4)
                        {
                              Common.LogADV("0", "0", "APP", Msisdn, Request.UserAgent.ToString(), Request.RawUrl.ToString(), Request.ServerVariables["REMOTE_ADDR"].ToString());
                        }
                  }
                  catch (Exception ex)
                  {
                        listArticles = null;
                        Response.Headers.Add("Exception",  ex.Message);
                        Response.Headers.Add("StackTrace", ex.StackTrace);
                  }


                  dataArticle = new DataArticle(listArticles);
                  string json = JsonConvert.SerializeObject(dataArticle);
                  Response.Write(json);
                  Response.End();
            }
      }
}