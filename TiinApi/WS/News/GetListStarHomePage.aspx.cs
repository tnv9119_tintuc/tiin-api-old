﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.News
{
      public partial class GetListStarHomePage : System.Web.UI.Page
      {
            public string Phone  = "";
            public string Msisdn = "";


            protected void Page_Load(object sender, EventArgs e)
            {
                  List<ListStarHome> item1 = new List<ListStarHome>();

                  DataListStarHome dataArticle = new DataListStarHome(null);

                  try
                  {
                        DataSet ds = null;
                        // chen ca sy
                        ListStarHome   item     = new ListStarHome();
                        List<ListStar> listStar = new List<ListStar>();
                        item.Name = "Ca sĩ";
                        item.Type = 3;

                        ds = Common.getListStar("3");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+  Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen Hoa hậu
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "Hoa hậu";
                        item.Type = 4;

                        ds = Common.getListStar("4");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen Đạo diễn
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "Đạo diễn";
                        item.Type = 5;

                        ds = Common.getListStar("5");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen MC
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "MC";
                        item.Type = 7;

                        ds = Common.getListStar("7");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen Nhạc sĩ
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "Nhạc sĩ";
                        item.Type = 10;

                        ds = Common.getListStar("10");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen Diễn viên
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "Diễn viên";
                        item.Type = 10;

                        ds = Common.getListStar("10");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        // chen Người mẫu
                        item      = new ListStarHome();
                        listStar  = new List<ListStar>();
                        item.Name = "Người mẫu";
                        item.Type = 17;

                        ds = Common.getListStar("17");
                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    ListStar itemValue = new ListStar();
                                    itemValue.Cid      = 0;
                                    itemValue.Pid      = 0;
                                    itemValue.Position = 0;
                                    itemValue.Reads    = 0;
                                    itemValue.Like     = 0;
                                    itemValue.LoaiBai = 0;
                                    itemValue.Url      = "";
                                    itemValue.VideoId = 0;
                                    itemValue.Title    = row["fullname"].ToString();
                                    itemValue.Image = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/')+ Path.AltDirectorySeparatorChar +
                                                      row["avatar_path"].ToString();
                                    itemValue.Id = Convert.ToInt32(row["id"].ToString());
                                    listStar.Add(itemValue);
                              }
                        }

                        item.ListStar = listStar;

                        item1.Add(item);


                        //Log dữ liệu
                        if ((HttpContext.Current.Session["MSISDN"] != null))
                        {
                              //lay so msisdn tu session
                              Msisdn = Session["MSISDN"].ToString();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                        }
                        else
                        {
                              Msisdn = Common.GetMSISDNOnHeader();
                              Msisdn = LibCRBT.CheckPhoneValidAll(Msisdn);
                              if (Msisdn.Length < 5)
                              {
                                    Phone                                 = Common.getPhoneNumber(Request);
                                    Msisdn                                = LibCRBT.CheckPhoneValidAll(Phone);
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                              else
                              {
                                    HttpContext.Current.Session["MSISDN"] = Msisdn;
                              }
                        }

                        if (Msisdn.Length > 4)
                        {
                              Common.LogADV("0", "0", "APP", Msisdn, Request.UserAgent.ToString(),
                                            Request.RawUrl.ToString(),
                                            Request.ServerVariables["REMOTE_ADDR"].ToString());
                        }
                  }
                  catch (Exception ex)
                  {
                        dataArticle.exception = ex;
                  }

                  dataArticle.data = item1;
                  Response.Json(dataArticle);
            }
      }
}