﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web;
using ViettelMedia.TiinApi.Commons;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.WS.Others
{
      public partial class GetSetting1 : System.Web.UI.Page
      {
            protected string phone  = "";
            protected string MSISDN = "";

            protected void Page_Load(object sender, EventArgs e)
            {
                  List<Setting> item1 = new List<Setting>();

                  DataGetSetting dataGetSetting = new DataGetSetting(null);

                  try
                  {
                        DataSet ds = Common.getSetting();


                        if (ds.Tables.Count > 0)
                        {
                              foreach (DataRow row in ds.Tables[0].Rows)
                              {
                                    Setting item = new Setting();
                                    item.AdmobBanner          = row["AdmobBaner"].ToString();
                                    item.AdmobFullScreen     = row["AdmobFullScreen"].ToString();
                                    item.AdmobBannerIos      = row["AdmobBaner_ios"].ToString();
                                    item.AdmobFullScreenIos = row["AdmobFullScreen_ios"].ToString();
                                    item.Advertise           = Convert.ToInt32(row["isActive"].ToString());
                                    item.AdvertiseIos       = Convert.ToInt32(row["isActive_ios"].ToString());
                                    item.AndroidVersion     = Convert.ToInt32(row["Android_versioncode"].ToString());
                                    item.IosVersion         = Convert.ToInt32(row["ios_versioncode"].ToString());
                                    item.TimeDelayAds      = Convert.ToInt32(row["Time_delay_ads"].ToString());


                                    item1.Add(item);
                              }


                              dataGetSetting.data = item1;

                              //Log dữ liệu
                              if ((HttpContext.Current.Session["MSISDN"] != null))
                              {
                                    //lay so msisdn tu session
                                    MSISDN = Session["MSISDN"].ToString();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                              }
                              else
                              {
                                    MSISDN = Common.GetMSISDNOnHeader();
                                    MSISDN = LibCRBT.CheckPhoneValidAll(MSISDN);
                                    if (MSISDN.Length < 5)
                                    {
                                          phone                                 = Common.getPhoneNumber(Request);
                                          MSISDN                                = LibCRBT.CheckPhoneValidAll(phone);
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                                    else
                                    {
                                          HttpContext.Current.Session["MSISDN"] = MSISDN;
                                    }
                              }

                              if (MSISDN.Length > 4)
                              {
                                    Common.LogADV("0", "0", "APP", MSISDN, Request.UserAgent.ToString(),
                                                  Request.RawUrl.ToString(),
                                                  Request.ServerVariables["REMOTE_ADDR"].ToString());
                              }
                        }
                  }
                  catch (Exception ex)
                  {
                        dataGetSetting.exception = ex;
                  }

                  Response.Json(dataGetSetting);
            }
      }
}