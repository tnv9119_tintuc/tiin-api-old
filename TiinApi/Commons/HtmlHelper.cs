using System.Collections.Generic;
using System.Linq;
using HtmlAgilityPack;

namespace ViettelMedia.TiinApi.Commons
{
    public static class HtmlHelper
    {
        public static void RemoveChildExcept(this HtmlNode baseNode, params string[] exceptTags)
        {
            if (baseNode != null)
            {
                var nodes = new Queue<HtmlNode>(baseNode.SelectNodes("./*|./text()").Cast<HtmlNode>().ToList());
                while (nodes.Count > 0)
                {
                    var node = nodes.Dequeue();
                    var parentNode = node.ParentNode;

                    if (parentNode != null)
                    {
                        if (exceptTags != null && (exceptTags.Contains(node.Name) || node.Name == "#text")) continue;
                        var childNodes = node.SelectNodes("./*|./text()");

                        if (childNodes != null)
                        {
                            foreach (var child in childNodes)
                            {
                                nodes.Enqueue(child);
                                parentNode.InsertBefore(child, node);
                            }
                        }

                        parentNode.RemoveChild(node);
                    }
                }
            }
        }

        public static bool HasClass(this HtmlNode node, string cssClass)
        {
            return node.HasAttributes &&
                   node.Attributes["class"] != null &&
                   node.Attributes["class"].Value.ToLower().Contains(cssClass.ToLower());
        }
    }
}