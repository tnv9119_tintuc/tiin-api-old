﻿using System;
using System.Data;
using ViettelMedia.TiinApi.Mocha.Models;

namespace ViettelMedia.TiinApi.Commons
{
    public static class Rewrite
    {
        /// <summary>
        /// Generate Url Detail for Tiin under structs
        /// </summary>
        /// <param name="container"></param>
        /// <returns></returns>
        public static string GenRewriteUrlDetail(DataRow container)
        {
            var link = "";
            var webRoot = Common.WebRoot.TrimEnd('/');
            try
            {
                var icon = container["icon"].ToString();

                switch (icon)
                {
                    case "0":
                    //Tin bài thông thường, ảnh slide
                    case "1":
                    {
                        var slug = container["slug"].ToString();
                        var categoryAlias = container["CategoryAlias"].ToString();

                        link = $"{webRoot}/chuyen-muc/{categoryAlias}/{slug}.html";
                        break;
                    }

                    //Ảnh
                    case "3":
                    {
                        var slug = container["slug"].ToString();
                        var categoryAlias = container["CategoryAlias"].ToString();
                        var id = container["id"].ToString();
                        link = $"{webRoot}/tin-anh/chuyen-muc/{categoryAlias}/{slug}/{id}.html";
                        break;
                    }

                    //Video
                    case "2":
                    {
                        var slug = container["slug"].ToString();
                        var categoryAlias = container["CategoryAlias"].ToString();
                        var id = container["id"].ToString();
                        link = $"{webRoot}/video/{categoryAlias}/{slug}/{id}.html";
                        break;
                    }

                    default:
                    {
                        var slug = container["slug"].ToString();
                        var categoryAlias = container["CategoryAlias"].ToString();
                        link = $"{webRoot}/chuyen-muc/{categoryAlias}/{slug}.html";
                        break;
                    }
                }
            }
            catch (Exception)
            {
                //HttpContext.Current.Response.Write(ex.Message);
            }

            return $"{link}?id={container["id"]}";
        }


        public static string GenRewriteUrlDetail(MochaArticle article)
        {
            string link;
            var webRoot = Common.WebRoot.TrimEnd('/');
            try
            {
                var icon = article.LoaiBai;

                switch (icon)
                {
                    case 0:
                    //Tin bài thông thường, ảnh slide
                    case 1:
                    {
                        var slug = article.Slug;
                        var categoryAlias = article.CategoryAlias ?? article.ParentCategoryAlias;
                        link = $"{webRoot}/chuyen-muc/{categoryAlias}/{slug}.html";
                        break;
                    }

                    //Ảnh
                    case 3:
                    {
                        var slug = article.Slug;
                        var categoryAlias = article.CategoryAlias ?? article.ParentCategoryAlias;
                        var id = article.Id;
                        link = $"{webRoot}/tin-anh/chuyen-muc/{categoryAlias}/{slug}/{id}.html";
                        break;
                    }

                    //Video
                    case 2:
                    {
                        var slug = article.Slug;
                        var categoryAlias = article.CategoryAlias ?? article.ParentCategoryAlias;
                        var id = article.Id;
                        link = $"{webRoot}/video/{categoryAlias}/{slug}/{id}.html";
                        break;
                    }

                    default:
                    {
                        var slug = article.Slug;
                        var categoryAlias = article.CategoryAlias ?? article.ParentCategoryAlias;
                        link = $"{webRoot}/chuyen-muc/{categoryAlias}/{slug}.html";
                        break;
                    }
                }
            }
            catch (Exception)
            {
                var slug = article.Slug;
                var categoryAlias = article.CategoryAlias ?? article.ParentCategoryAlias;
                link = $"{webRoot}/chuyen-muc/{categoryAlias}/{slug}.html";
            }

            return link + "?id=" + article.Id.ToString();
        }
    }
}