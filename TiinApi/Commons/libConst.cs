using System.Configuration;

namespace ViettelMedia.TiinApi.Commons
{
      public class LibConst
      {
            public static string PhoneSyntax    = "0123456789";
            public static string WsPhoneViettel = ConfigurationManager.AppSettings["wsPhoneViettel"];
            public static string WsPhone        = ConfigurationManager.AppSettings["wsPhoneExt"];
      }
}