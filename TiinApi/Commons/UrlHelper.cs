using System;

namespace ViettelMedia.TiinApi.Commons
{
      /// <summary>
      /// Helpers for Url
      /// Coded by © Nha.HV <nhahv@tiin.vn>
      /// </summary>
      public static class UrlHelper
      {
            /// <summary>
            /// URL Normalize
            /// Remove all unnecessary character from url
            /// </summary>
            /// <param name="url"></param>
            /// <returns></returns>
            public static string NormalizeUrl(string url)
            {
                  return url.Replace('\\', '/').TrimEnd('/');
            }

            /// <summary>
            /// Path Normalize
            /// Change URL Slash to Path Slash
            /// </summary>
            /// <param name="path"></param>
            /// <returns></returns>
            public static string NormalizePath(string path)
            {
                  return path.Replace('/', '\\').TrimEnd('\\');
            }
      }

      /// <summary>
      /// Helpers for String
      /// Coded by © Nha.HV <nhahv@tiin.vn>
      /// </summary>
      public static class StringHelper
      {
            public static string RemoveLastSlash(this string strUrl)
            {
                  return strUrl.TrimEnd('/');
            }

            public static string AddSlash(this string strUrl)
            {
                  return strUrl.TrimEnd('/');
            }

            public static string Append(this string str, string appendStr)
            {
                  return str + appendStr;
            }
            
            
            public static string WithMaxLength(this string value, int maxLength)
            {
                  return value?.Substring(0, Math.Min(value.Length, maxLength));
            }
      }
}