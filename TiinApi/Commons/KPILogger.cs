using System;
using System.Collections.Specialized;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ViettelMedia.TiinApi.Commons
{
    public class KPILogger
    {
        public static T LogKPI<T>(string name, Func<T> func)
        {
            var logPath = !string.IsNullOrEmpty(ConfigurationManager.AppSettings["TiinKPILogPath"])
                ? ConfigurationManager.AppSettings["TiinKPILogPath"]
                : HttpContext.Current.Request.MapPath("~/KPILOG");


            var stopWatch = new Stopwatch();
            stopWatch.Start();
            var data = func.Invoke();
            stopWatch.Stop();
            TimeSpan ts = stopWatch.Elapsed;
            string elapsedTime = "" + ts.TotalMilliseconds;

            try
            {
                //clientType=Android&Platform=Mocha&revision=15162 

                var platform = HttpContext.Current.Request.QueryString.Get("Platform");
                var clientType = HttpContext.Current.Request.QueryString.Get("clientType");
                var revision = HttpContext.Current.Request.QueryString.Get("revision");
                var msisdn = HttpContext.Current.Request.QueryString.Get("msisdn");
                var loginStatus = HttpContext.Current.Request.QueryString.Get("loginStatus");

                NameValueCollection qs = new NameValueCollection(HttpContext.Current.Request.QueryString);
                qs.Remove("Platform");
                qs.Remove("clientType");
                qs.Remove("revision");
                qs.Remove("loginStatus");
                qs.Remove("msisdn");

                var logContent = String.Join("|", DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"), elapsedTime,
                    platform, clientType, revision, msisdn, loginStatus, name, HttpContext.Current.Request.Url);


                Console.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"));
                Console.WriteLine(DateTime.Now.Hour.ToString());
                Console.WriteLine(DateTime.Now.Minute.ToString());
                var logKpi = String.Join("|",
                    Environment.MachineName, "ApiTiinOnMocha",
                    name,
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    DateTime.Now.Hour.ToString(),
                    DateTime.Now.Minute.ToString(),
                    msisdn,
                    elapsedTime);
                //tiinServiceKPI.2019-07-22.log.gz
                var logKpiFilePath =
                    $"{logPath}/tiinServiceKPI.{Environment.MachineName}.{DateTime.Now:yyyy-MM-dd}.log";
                var logFilePath = $"{logPath}/{DateTime.Now:yyyy-MM-dd}.txt";
                var logFileDir = Path.GetDirectoryName(logFilePath);
                if (!Directory.Exists(logFileDir))
                {
                    Directory.CreateDirectory(logFileDir ?? throw new IOException());
                }

                WriteLog(logKpiFilePath, logKpi);
                WriteLog(logFilePath, logContent);
            }
            catch (Exception e)
            {
                try
                {
                    var errKpiFilePath =
                        $"{logPath}/tiinServiceKPI.{Environment.MachineName}.{DateTime.Now:yyyy-MM-dd}.err";
                    var logFileDir = Path.GetDirectoryName(errKpiFilePath);
                    if (!Directory.Exists(logFileDir))
                    {
                        Directory.CreateDirectory(logFileDir ?? throw new InvalidOperationException());
                    }

                    var errMsg =
                        $@"{DateTime.Now:yyyy-MM-dd HH:mm:ss} : {e.Message}  {Environment.NewLine} {e.StackTrace} {Environment.NewLine} " +
                        $@"----------------------------------------------------------- {Environment.NewLine}";

                    WriteLog(errKpiFilePath, errMsg);
                }
                catch (Exception)
                {
                    //ignore
                }
            }

            return data;
        }

        private static async Task WriteLog(string filePath, string content)
        {
            using (var mutex = new Mutex(false, StringUtils.Md5Hash(filePath)))
            {
                mutex.WaitOne();
                if (!File.Exists(filePath))
                {
                    File.WriteAllText(filePath, content + Environment.NewLine);
                }

                File.AppendAllText(filePath, content + Environment.NewLine);
                mutex.ReleaseMutex();
            }
        }


        public static string ConvertToQueryString(NameValueCollection queryParameters)
        {
            var logKeys =
                "article_id,category_id,cid,id,isVip,keywords,num,page,pid,radio_id,star_id,thematic_id,type_id,unixTime,video_id,slug,url"
                    .Split(',');
            return string.Join("&", queryParameters.AllKeys
                .Where(k => logKeys.Contains(k.ToLower()))
                .Select(k =>
                    $"{HttpUtility.UrlEncode(k)}={HttpUtility.UrlEncode(queryParameters[k]).WithMaxLength(256)}")
            );
        }
    }
}