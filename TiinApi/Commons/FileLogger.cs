using System;
using System.Configuration;
using System.IO;
using System.Text;
using System.Web;

namespace ViettelMedia.TiinApi.Commons
{
    public class FileLogger : IDisposable
    {
        private static FileLogger _instance = null;

        private static readonly object _mutex = new object();

        private readonly FileStream _streamWriter;

        private FileLogger(string filePath)
        {
            var logFileDir = Path.GetDirectoryName(filePath);
            if (!Directory.Exists(logFileDir))
            {
                Directory.CreateDirectory(logFileDir ?? throw new InvalidOperationException());
            }
            
            if (!File.Exists(filePath))
            {
                File.Create(filePath);
            }

            _streamWriter = new FileStream(filePath, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);
        }

        public static FileLogger Instance(string filePath)
        {
            if (_instance == null)
            {
                lock (_mutex) // now I can claim some form of thread safety...
                {
                    if (_instance == null)
                    {
                        _instance = new FileLogger(filePath);
                    }
                }
            }

            return _instance;
        }

        public void WriteLine(string content)
        {
            byte[] info = new UTF8Encoding(true).GetBytes(content + Environment.NewLine);
            _streamWriter.Write(info, 0, info.Length);
        }

        public void Dispose()
        {
            _streamWriter.Dispose();
        }
    }
}