﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using LukeSkywalker.IPNetwork;
using Memcached.ClientLibrary;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using TiinApi;
using vtwap.vt.ptnd.Radius;
using ViettelMedia.TiinApi.Extensions;
using ViettelMedia.TiinApi.Models;

//using Memcached.ClientLibrary;
//using Microsoft.ApplicationBlocks.Data;

namespace ViettelMedia.TiinApi.Commons
{
    public class Common
    {
        public static string WebRoot = ConfigurationManager.AppSettings["WebRoot"].TrimEnd('/') +
                                       Path.AltDirectorySeparatorChar;

        public static string MediaRoot = ConfigurationManager.AppSettings["MediaRoot"].TrimEnd('/') +
                                         Path.AltDirectorySeparatorChar;

        public static string MediaRootNew = ConfigurationManager.AppSettings["MediaRootNew"].TrimEnd('/') +
                                            Path.AltDirectorySeparatorChar;

        public static int SecondsCache = Convert.ToInt32(ConfigurationManager.AppSettings["SecondsCache"]);
        public static int MinutesCache = Convert.ToInt32(ConfigurationManager.AppSettings["MinutesCache"]);

        public static string Memcached2 = ConfigurationManager.AppSettings["Memcached2"];
        public static string Memcached1 = ConfigurationManager.AppSettings["Memcached1"];
        public static string Memcached = ConfigurationManager.AppSettings["Memcached"];

        public static string DefaultSourceName =
            GetValueOrDefault(ConfigurationManager.AppSettings["DefaultSourceName"], "Baodatviet.vn");

        public static string DefaultAuthorName =
            GetValueOrDefault(ConfigurationManager.AppSettings["DefaultAuthorName"], "PV");
        
        public static string VERSION_PREFIX =
            GetValueOrDefault(ConfigurationManager.AppSettings["VersionPrefix"], "1.0");

        #region "QueryString"

        public static string GetValueSimple(string name, string defaultValue)
        {
            string res = (HttpContext.Current.Request.QueryString[name] == null
                ? defaultValue
                : HttpContext.Current.Request.QueryString[name]);

            if (res.Length > 300)
                res = defaultValue;

            return res.Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetValueExxx(string name, string defaultValue)
        {
            string res = (HttpContext.Current.Request.QueryString[name] == null
                ? defaultValue
                : HttpContext.Current.Request.QueryString[name]);

            if (res.Length > 128)
                res = defaultValue;

            return res.Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetValueEx(string name, string defaultValue)
        {
            string res = (HttpContext.Current.Request.QueryString[name] == null
                ? defaultValue
                : HttpContext.Current.Request.QueryString[name]);

            if (res.Length > 30)
                res = defaultValue;

            res = ValidateXSS(res.ToLower());

            return res.Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetValue(string name, string defaultValue)
        {
            string res = (HttpContext.Current.Request.QueryString[name] == null
                ? defaultValue
                : HttpContext.Current.Request.QueryString[name]);
            res = ValidateXSS(res);
            //res = KillChars(res);

            if (res.Length > 20)
                res = defaultValue;

            return res.Trim();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="inputString"></param>
        /// <returns></returns>
        public static bool IsNumeric(string inputString)
        {
            try
            {
                return Regex.IsMatch(inputString, "^[0-9]+$");
            }
            catch
            {
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strWords"></param>
        /// <returns></returns>
        public static string ValidateXSS(string strWords)
        {
            StringBuilder sb = new StringBuilder(HttpUtility.HtmlEncode(strWords));

            string[] badChars = {"<", ">", "/", "script", "iframe"};

            string newChars = sb.ToString();

            foreach (string str in badChars)
            {
                newChars = Regex.Replace(newChars, str, "", RegexOptions.IgnoreCase);
            }

            return newChars;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="strWords"></param>
        /// <returns></returns>
        public static string KillChars(string strWords)
        {
            //News
            string[] badChars =
            {
                "=",
                "xp_",
                ";",
                "--",
                "<",
                ">",
                "script",
                "iframe",
                "delete",
                "drop",
                "exec",
                "insert",
                "'",
                "/\\*",
                "\\*/",
                "--",
                ";--",
                ";",
                "@@",
                "@",
                "char",
                "nchar",
                "varchar",
                "nvarchar",
                "alter",
                "begin",
                "cast",
                "create",
                "cursor",
                "declare",
                "delete",
                "drop",
                "end",
                "exec",
                "execute",
                "fetch",
                "insert",
                "kill",
                "open",
                "select",
                "sys",
                "sysobjects",
                "syscolumns",
                "table",
                "update"
            };

            string newChars = strWords.ToLower();

            foreach (string str in badChars)
            {
                newChars = Regex.Replace(newChars, str, "", RegexOptions.IgnoreCase);
            }

            newChars = newChars.Replace("[", "");

            //Allow a-b, 0-9, space, manhlq@gmail.com
            newChars = Regex.Replace(newChars, @"[^a-zA-Z0-9 ]", " ", RegexOptions.Compiled);

            return newChars;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <returns></returns>
        public static string ValidInputQueryString(string input)
        {
            return Regex.Replace(input, @"[^a-zA-Z0-9 ]", " ", RegexOptions.Compiled);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="input"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string ValidateInt(string input, string defaultValue)
        {
            if (input == null)
                return defaultValue;

            Regex objNotWholePattern = new Regex("[^0-9]");
            if (!objNotWholePattern.IsMatch(input) && (input != ""))
                return input;
            else
                return defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="name"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static string GetValueInt(string name, string defaultValue)
        {
            string res = GetValue(name, defaultValue);
            return ValidateInt(res, defaultValue);
        }

        public static T GetValueOrDefault<T>(T obj, T defaultValue)
        {
            if (obj == null || (obj is string && string.IsNullOrEmpty(obj as string)))
            {
                return defaultValue;
            }

            return obj;
        }

        #endregion

        #region "Common"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="_input"></param>
        /// <param name="number_word"></param>
        /// <returns></returns>
        public static string LimitString(object _input, int number_word)
        {
            string input = (string) _input;
            string output = "";
            if (input.Length > 50)
            {
                string[] elements = input.Split(' ');

                if (elements.Length > number_word)
                {
                    for (int i = 0; i < number_word; i++)
                    {
                        output += elements[i] + " ";
                    }

                    output += "...";
                }
                else
                {
                    output = input;
                }
            }
            else
            {
                output = input;
            }

            return output;
        }

        #endregion

        #region "Private"

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public int GetPidFromSession()
        {
            int pid = 0;
            if (pid == 0)
            {
                if (HttpContext.Current.Session["pid"] != null)
                {
                    pid = int.Parse(HttpContext.Current.Session["pid"].ToString());
                }
            }

            return pid;
        }

        #endregion

        #region "DAL"

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string getConnectionString()
        {
            return ConfigurationManager.AppSettings["cnString"];
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="model"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxCate_1BigImg_3TitleUnder(int cid)
        {
            var cacheName =
                String.Join("_", new String[] {"Home_GetBoxCate_1BigImg_3TitleUnder", cid.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$homeCate", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxCate_3BigImg_3TitleUnder(int cid)
        {
            var cacheName =
                String.Join("_", new string[] {"Home_GetBoxCate_3BigImg_3TitleUnder", cid.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$homeCate1", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxMostView(int pid, int cid)
        {
            var cacheName =
                String.Join("_", new string[] {"Home_GetBoxMostView", pid.ToString(), cid.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$MostView", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxTinMoi(int pid, int cid)
        {
            var cacheName =
                String.Join("_", new string[] {"Home_GetBoxTinMoi", pid.ToString(), cid.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure, "Tiin_Web$New",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxTinHot(int pid, int cid)
        {
            var cacheName =
                String.Join("_", new[] {"Home_GetBoxTinHot", pid.ToString(), cid.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure, "Tiin_Web$Hot",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxBackLink()
        {
            var cacheName =
                String.Join("_", new[] {"Home_GetBoxBackLink"});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$BackLink", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxProductsAndServices()
        {
            var cacheName =
                String.Join("_", new[] {"Home_GetBoxProductsAndServices"});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$ProductsAndServices", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet getListArticleHomepage()
        {
            var cacheName =
                String.Join("_", new[] {"getListArticleHomepage"});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure, "Tiin_API$home",
                            parameters);
                });
        }

        public static DataSet Tiin_API_Mocha_HomePage()
        {
            SqlParameter[] parameters = { };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$HomePage",
                    parameters);
        }

        public static DataSet Tiin_API_Mocha_HotTopic(string page, string num)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("page", SqlDbType.Int).SetValue(page),
                new SqlParameter("pageSize", SqlDbType.Int).SetValue(num),
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$HotTopic",
                    parameters);
        }

        public static DataSet Tiin_API_Mocha_HotTopic_Articles(string thematic_id, string page, string num)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("thematic_id", SqlDbType.Int) {Value = thematic_id},
                new SqlParameter("page", SqlDbType.Int).SetValue(page),
                new SqlParameter("pageSize", SqlDbType.Int).SetValue(num),
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$HotTopicArticles",
                    parameters);
        }

        public static DataSet Tiin_API_Mocha_Thematic_Articles(string post_id, string page, string num)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("post_id", SqlDbType.Int) {Value = post_id},
                new SqlParameter("page", SqlDbType.Int).SetValue(page),
                new SqlParameter("pageSize", SqlDbType.Int).SetValue(num),
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$ThematicArticles",
                    parameters);
        }


        public static DataSet Tiin_API_Mocha_MostView(string pid = "0", string cid = "0", string page = "1",
            string num = "10")
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("pid", SqlDbType.Int) {Value = pid},
                new SqlParameter("cid", SqlDbType.Int) {Value = cid},
                new SqlParameter("page", SqlDbType.Int).SetValue(page),
                new SqlParameter("pageSize", SqlDbType.Int).SetValue(num),
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$MostView",
                    parameters);
        }

        public static DataSet Tiin_API_Mocha_LatestNews(string pid, string cid, string page, string num)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("pid", SqlDbType.Int).SetValue(pid),
                new SqlParameter("cid", SqlDbType.Int).SetValue(cid),
                new SqlParameter("page", SqlDbType.Int).SetValue(page),
                new SqlParameter("pageSize", SqlDbType.Int).SetValue(num),
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Tiin_API$Mocha$LatestNews",
                    parameters);
        }

        public static DataSet Tiin_API_Mocha_HomePage_Cate(string cateId)
        {
            // Tiin_Web$homeCate1
            var cacheName =
                String.Join("_", new[] {"Tiin_API$Mocha$HomePageCate", cateId});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(cateId),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure, "Tiin_API$Mocha$HomePageCate",
                            parameters);
                });
        }

        public static DataSet getListCategory()
        {
            DataSet ds = null;

            var cacheName = "getListCategory";

            ds = CacheManager.Remember(
                cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                () => SqlHelper
                    .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                        CommandType
                            .StoredProcedure, "Tiin_API$Category"));
            return ds;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getArticleOfTinMedia(string page, string number)
        {
            var cacheName =
                String.Join("_", new[] {"getArticleOfTinMedia", page, number});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(100),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(0),
                        new SqlParameter("@pageIndex", SqlDbType.Int).SetValue(page),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$CateAnh", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="category_id"></param>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getArticleOfCategory(string category_id, string page, string number)
        {
            string cacheName = "getArticleOfCategory_" + category_id + "_" + page + "_" + number;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@category_id", SqlDbType.Int),
                        new SqlParameter("@number", SqlDbType.Int),
                        new SqlParameter("@pageIndex", SqlDbType.Int),
                    };
                    parameters[0].Value = category_id;
                    parameters[1].Value = number;
                    parameters[2].Value = page;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Cate", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="type_id"></param>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getListStarByType(string type_id, string page, string number)
        {
            string cacheName = "getListStarByType_" + type_id + "_" + page + "_" + number;

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@type_id", SqlDbType.Int),
                        new SqlParameter("@pageIndex", SqlDbType.Int),
                    };
                    parameters[0].Value = type_id;
                    parameters[1].Value = page;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$CateStart",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="thematic_id"></param>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getArticleOfChuyenDe(string thematic_id, string page, string number)
        {
            string cacheName = "getArticleOfChuyenDe_" + thematic_id + "_" + page + "_" + number;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@thematic_id", SqlDbType.Int),
                        new SqlParameter("@number", SqlDbType.Int),
                        new SqlParameter("@pageIndex", SqlDbType.Int),
                    };
                    parameters[0].Value = thematic_id;
                    parameters[1].Value = number;
                    parameters[2].Value = page;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Thematic",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getHomepageArticleImage(string page, string number)
        {
            string cacheName = "getHomepageArticleImage_" + page + "_" + number;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@number", SqlDbType.Int),
                        new SqlParameter("@pageIndex", SqlDbType.Int),
                    };
                    parameters[0].Value = number;
                    parameters[1].Value = page;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$CateImage",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="article_id"></param>
        /// <returns></returns>
        public static DataSet getArticleDetail(string article_id)
        {
            string cacheName = "getArticleDetail:" + article_id;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int),
                    };
                    parameters[0].Value = article_id;

                    var dataset = SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$ArticleDetail",
                            parameters);

                    try
                    {
                        if (dataset.Tables.Count > 0)
                        {
                            foreach (DataRow row in dataset.Tables[0].Rows)
                            {
                                row["Content"] = AppendIdToUrl(row["Content"].ToString());
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        HttpContext.Current.Response.Headers.Add("Error", "Error AppendIdToUrl: " + exception.Message);
                    }

                    return dataset;
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="articleId"></param>
        /// <returns></returns>
        public static DataSet GetArticleFastRead(string articleId, string slug)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int) {Value = articleId},
                new SqlParameter("@slug", SqlDbType.NVarChar) {Value = slug},
            };

            var dataset = SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure,
                    "Tiin_API$ArticleFastRead",
                    parameters);

            try
            {
                if (dataset.Tables.Count > 0)
                {
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        row["Content"] = AppendIdToUrl(row["Content"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                HttpContext.Current.Response.Headers.Add("Error", "Error AppendIdToUrl: " + exception.Message);
            }

            return dataset;
        }

        public static DataSet MochaGetVideoDetail(string articleId)
        {
            string cacheName = "TiinAPI:GetVideoDetail:" + articleId;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int),
                    };
                    parameters[0].Value = articleId;

                    var dataset = SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Mocha$GetVideoDetail",
                            parameters);

                    try
                    {
                        if (dataset.Tables.Count > 0)
                        {
                            foreach (DataRow row in dataset.Tables[0].Rows)
                            {
                                row["Content"] = AppendIdToUrl(row["Content"].ToString());
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        HttpContext.Current.Response.Headers.Add("Error", "Error AppendIdToUrl: " + exception.Message);
                    }

                    return dataset;
                });
        }

        public static DataSet GetArticleSiblings(string articleId, string page, string number)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter {ParameterName = "@id", DbType = DbType.Int32, Value = articleId},
                new SqlParameter {ParameterName = "@page", DbType = DbType.Int32, Value = page},
                new SqlParameter {ParameterName = "@num", DbType = DbType.Int32, Value = number},
            };
            parameters[0].Value = articleId;

            var dataset = SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure,
                    "Tiin_API$ArticleSiblings",
                    parameters);

            try
            {
                if (dataset.Tables.Count > 0)
                {
                    foreach (DataRow row in dataset.Tables[0].Rows)
                    {
                        row["Content"] = AppendIdToUrl(row["Content"].ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                HttpContext.Current.Response.Headers.Add("Error", "Error AppendIdToUrl: " + exception.Message);
            }

            return dataset;
        }

        public static DataSet GetListArticleOfMessageType(int type, string page, string number)
        {
            string cacheName = "TiinAPI:GetListArticleOfMessageType:" + type + ":" + page + ":" + number;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter {ParameterName = "@type", DbType = DbType.Int32, Value = type},
                        new SqlParameter {ParameterName = "@page", DbType = DbType.Int32, Value = page},
                        new SqlParameter {ParameterName = "@num", DbType = DbType.Int32, Value = number},
                    };

                    var dataset = SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$GetListArticleOfMessageType",
                            parameters);

                    try
                    {
                        if (dataset.Tables.Count > 0)
                        {
                            foreach (DataRow row in dataset.Tables[0].Rows)
                            {
                                row["Content"] = AppendIdToUrl(row["Content"].ToString());
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        HttpContext.Current.Response.Headers.Add("Error", "Error AppendIdToUrl: " + exception.Message);
                    }

                    return dataset;
                });
        }

        /// <summary>
        /// Trả về tin liên quan của bài viết
        /// </summary>
        /// <param name="article_id"></param>
        /// <returns></returns>
        public static DataSet getArticleRelatedNews(string article_id)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int),
            };
            parameters[0].Value = article_id;

            var dataset = SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure,
                    "Tiin_API$ArticleRelatedNews",
                    parameters);
            return dataset;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="article_id"></param>
        /// <returns></returns>
        public static DataSet getListVideoInTruongQuayTiin(string article_id)
        {
            string cacheName = "getListVideoInTruongQuayTiin_" + article_id;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int),
                    };
                    parameters[0].Value = article_id;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$ListVideoInTruongQuayTiin",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="article_id"></param>
        /// <returns></returns>
        public static DataSet getPhotoListOfArticle(string article_id)
        {
            string cacheName = "getPhotoListOfArticle_" + article_id;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int),
                    };
                    parameters[0].Value = article_id;

                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$ReadImage",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="keywords"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public static DataSet searchDetail(string keywords, string pageIndex)
        {
            var cacheName = "searchDetail_" + keywords + "_" + pageIndex;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@keywords", SqlDbType.NVarChar)
                            .SetValue(keywords),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(pageIndex)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Search",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public static DataSet getListStar(string id)
        {
            string cacheName = "getListStar_" + id;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$HomeStart",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="star_id"></param>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public static DataSet getListArticleOfStar(string star_id, string pageIndex)
        {
            string cacheName = "getListArticleOfStar_" + star_id + "_" + pageIndex;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@star_id", SqlDbType.Int)
                            .SetValue(star_id),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(pageIndex)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$ArticleOfStar",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <returns></returns>
        public static DataSet getArticleOfHoroscope(string pageIndex)
        {
            string cacheName = "getArticleOfHoroscope_" + pageIndex;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(pageIndex)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Horoscope",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="munber"></param>
        /// <returns></returns>
        public static DataSet getListRadioHomepage(string pageIndex, string munber)
        {
            string cacheName = "getListRadioHomepage_" + pageIndex + "_" + munber;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@number", SqlDbType.Int)
                            .SetValue(munber),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(pageIndex),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$CateRadio",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pageIndex"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getListVideoHomepage(string pageIndex, string number)
        {
            string cacheName = "getListVideoHomepage_" + pageIndex + "_" + number;

            MemcachedClient mc = new MemcachedClient();
            mc.EnableCompression = false;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@number", SqlDbType.Int)
                            .SetValue(number),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(pageIndex)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$CateVideo",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="radio_id"></param>
        /// <returns></returns>
        public static DataSet getRadioDetail(string radio_id)
        {
            string cacheName = "getRadioDetail_" + radio_id;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@radio_id", SqlDbType.Int)
                            .SetValue(radio_id)
                    };
                    var data = SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$RadioDetail",
                            parameters);
                    return data;
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="video_id"></param>
        /// <returns></returns>
        public static DataSet getVideoDetail(string video_id, string page, string num)
        {
            string cacheName = "getVideoDetail_" + video_id + "_page_" + page + "_num_" + num;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@video_id", SqlDbType.Int)
                            .SetValue(video_id),
                        new SqlParameter("@num", SqlDbType.Int)
                            .SetValue(num),
                        new SqlParameter("@page", SqlDbType.Int)
                            .SetValue(page)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$VideoDetail",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="wifi_mac"></param>
        /// <param name="push_id"></param>
        /// <param name="device_type"></param>
        /// <param name="msisdn"></param>
        /// <param name="device_id"></param>
        /// <returns></returns>
        public static DataSet SetRegDevice(string wifi_mac, string push_id, string device_type, string msisdn,
            string device_id)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["cnLogString"]))
            {
                SqlCommand cmd = new SqlCommand();
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "app_ListRegistration$Set";
                cmd.Parameters.AddWithValue("@wifi_mac", wifi_mac);
                cmd.Parameters.AddWithValue("@push_id", push_id);
                cmd.Parameters.AddWithValue("@device_type", device_type);
                cmd.Parameters.AddWithValue("@msisdn", msisdn);
                cmd.Parameters.AddWithValue("@device_id", device_id);

                conn.Open();
                cmd.CommandTimeout = 5;
                using (SqlDataAdapter reader = new SqlDataAdapter(cmd))
                {
                    DataSet ds = new DataSet();
                    reader.Fill(ds);
                    return ds;
                }
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet getSetting()
        {
            string cacheName = "getSetting";
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$Getsetting",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="star_id"></param>
        /// <returns></returns>
        public static DataSet getProfileOfStar(string star_id)
        {
            string cacheName = "getProfileOfStar_" + star_id;

            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int)
                            .SetValue(star_id)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$getProfileOfStar",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="category_id"></param>
        /// <param name="page"></param>
        /// <param name="number"></param>
        /// <returns></returns>
        public static DataSet getArticleOfCategoryHaveContent(string category_id, string page, string number,
            long unixTime = 0)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@category_id", SqlDbType.Int)
                    .SetValue(category_id),
                new SqlParameter("@number", SqlDbType.Int)
                    .SetValue(number),
                new SqlParameter("@pageIndex", SqlDbType.Int)
                    .SetValue(page),
                new SqlParameter("@unixTime", SqlDbType.Int)
                    .SetValue(unixTime)
            };
            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure,
                    "Tiin_API$CateContent",
                    parameters);
        }

        public static DataSet getThematicList(string page, string number)
        {
            string cacheName =
                "getThematicList" + "_" + page + "_" + number;
            return CacheManager.Remember<DataSet>(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@page", SqlDbType.Int)
                            .SetValue(number),
                        new SqlParameter("@pageSize", SqlDbType.Int)
                            .SetValue(page)
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_API$GetThematicList",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxVideoHomeLeft()
        {
            return CacheManager.Remember<DataSet>("Home_GetBoxVideoHomeLeft",
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$HomeCateVideo");
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxVideoCate(int pid, int cid, string alias)
        {
            var cacheName =
                String.Join("_",
                    new string[] {"Home_GetBoxVideoCate", pid.ToString(), cid.ToString(), alias.ToString()});
            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@alias", SqlDbType.NVarChar)
                            .SetValue(alias),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$HomeVideo", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="index"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxVideoCateLeft(int pid, int cid, int index, string alias)
        {
            var cacheName =
                String.Join("_", new[]
                {
                    "Cate_GetBoxVideoCateLeft",
                    pid.ToString(),
                    cid.ToString(),
                    index.ToString(),
                    alias
                });
            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                        new SqlParameter("@alias", SqlDbType.NVarChar)
                            .SetValue(alias),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$CateVideo", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetMenuTop()
        {
            return CacheManager.Remember("Home_GetMenuTop",
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Header", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Home_GetBoxImageSlider(int pid, int cid)
        {
            var cacheName = String.Join("_", new[] {"Home_GetBoxImageSlider", pid.ToString(), cid.ToString()});
            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$SlideImage", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxLeft(int pid, int cid, int index, string alias)
        {
            var cacheName =
                String.Join("_",
                    new[] {"Cate_GetBoxLeft", pid.ToString(), cid.ToString(), index.ToString(), alias});
            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int),
                        new SqlParameter("@cid", SqlDbType.Int),
                        new SqlParameter("@pageIndex", SqlDbType.Int),
                        new SqlParameter("@alias", SqlDbType.NVarChar),
                    };
                    parameters[0].Value = pid;
                    parameters[1].Value = cid;
                    parameters[2].Value = index;
                    parameters[3].Value = alias;
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure, "Tiin_Web$Cate",
                            parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxLeftRight(int pid, int cid, string alias)
        {
            var cacheName =
                String.Join("_", new[] {"Cate_GetBoxLeftRight", pid.ToString(), cid.ToString(), alias});
            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int),
                        new SqlParameter("@cid", SqlDbType.Int),
                        new SqlParameter("@alias", SqlDbType.NVarChar),
                    };
                    parameters[0].Value = pid;
                    parameters[1].Value = cid;
                    parameters[2].Value = alias;
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$LeftCate", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxImageLeft(int pid, int cid, int index, string alias)
        {
            var cacheName =
                String.Join("_", new[]
                {
                    "Cate_GetBoxImageLeft",
                    pid.ToString(),
                    cid.ToString(),
                    index.ToString(),
                    alias
                });

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                        new SqlParameter("@alias", SqlDbType.NVarChar)
                            .SetValue(alias),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$CateImage", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="id"></param>
        /// <param name="slug"></param>
        /// <returns></returns>
        public static DataSet Detail_ReadNews(int pid, int cid, int id, string slug)
        {
            var cacheName =
                String.Join("_", new[]
                {
                    "Detail_ReadNews",
                    pid.ToString(),
                    cid.ToString(),
                    id.ToString(),
                    slug
                });

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id),
                        new SqlParameter("@slug", SqlDbType.NVarChar).SetValue(slug),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$ReadNews", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="id"></param>
        /// <param name="slug"></param>
        /// <param name="alias"></param>
        /// <returns></returns>
        public static DataSet Detail_ReadImage(int pid, int cid, int id, string slug, string alias)
        {
            var cacheName =
                String.Join("_", new[]
                {
                    "Detail_ReadImage",
                    pid.ToString(),
                    cid.ToString(),
                    id.ToString(),
                    slug,
                    alias
                });

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id),
                        new SqlParameter("@slug", SqlDbType.NVarChar).SetValue(slug),
                        new SqlParameter("@alias", SqlDbType.NVarChar)
                            .SetValue(alias),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$ReadImage", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="pid"></param>
        /// <param name="cid"></param>
        /// <param name="id"></param>
        /// <param name="slug"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static DataSet Detail_ReadVideo(int pid, int cid, int id, string slug, int index)
        {
            var cacheName =
                String.Join("_", new[]
                {
                    "Detail_ReadVideo",
                    pid.ToString(),
                    cid.ToString(),
                    id.ToString(),
                    slug,
                    index.ToString()
                });

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddSeconds(Common.SecondsCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pid", SqlDbType.Int).SetValue(pid),
                        new SqlParameter("@cid", SqlDbType.Int).SetValue(cid),
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id),
                        new SqlParameter("@slug", SqlDbType.NVarChar).SetValue(slug),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$ReadVideo", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxImage()
        {
            var cacheName =
                String.Join("_", new[] {"Home_GetBoxImage"});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$HomeImage", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DataSet Home_GetBoxVideoAndRadio()
        {
            var cacheName =
                String.Join("_", new[] {"Home_GetBoxVideoAndRadio"});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters = { };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$HomeVideoAndRadio", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="alias"></param>
        /// <param name="slug"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static DataSet ChuyenDe_GetBoxLeft(string alias, string slug, int index)
        {
            var cacheName =
                String.Join("_", new[] {"ChuyenDe_GetBoxLeft", alias, slug, index.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@alias", SqlDbType.NVarChar)
                            .SetValue(alias),
                        new SqlParameter("@slug", SqlDbType.NVarChar).SetValue(slug),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Thematic", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="index"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxTags(int id, int index, string name)
        {
            var cacheName =
                String.Join("_", new[] {"Cate_GetBoxTags", name, id.ToString(), index.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                        new SqlParameter("@name", SqlDbType.NVarChar).SetValue(name),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Tags", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="keyword"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxSearch(int index, string keyword)
        {
            var cacheName =
                String.Join("_", new[] {"Cate_GetBoxSearch", index.ToString(), keyword});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                        new SqlParameter("@text", SqlDbType.NVarChar)
                            .SetValue(keyword),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Search", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public static DataSet Cate_GetBoxCateTruongQuayTiin(int index)
        {
            var cacheName =
                String.Join("_", new[] {"Cate_GetBoxCateTruongQuayTiin", index.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Cate_truong_quay_tin", parameters);
                });
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="index"></param>
        /// <param name="slug"></param>
        /// <returns></returns>
        public static DataSet Detail_GetBoxReadTruongQuayTiin(int id, string slug, int index)
        {
            var cacheName =
                String.Join("_",
                    new[] {"Detail_GetBoxReadTruongQuayTiin", id.ToString(), slug, index.ToString()});

            return CacheManager.Remember(cacheName,
                DateTime.Now.AddMinutes(Common.MinutesCache),
                delegate
                {
                    SqlParameter[] parameters =
                    {
                        new SqlParameter("@id", SqlDbType.Int).SetValue(id),
                        new SqlParameter("@slug", SqlDbType.NVarChar).SetValue(slug),
                        new SqlParameter("@pageIndex", SqlDbType.Int)
                            .SetValue(index),
                    };
                    return SqlHelper
                        .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                            CommandType.StoredProcedure,
                            "Tiin_Web$Read_truong_quay_tin", parameters);
                });
        }

        public static DataSet GetIdsFromSlugs(string strSlugs)
        {
            SqlParameter[] parameters =
            {
                new SqlParameter("@slugs", SqlDbType.NVarChar).SetValue(strSlugs),
            };

            return SqlHelper
                .ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType
                        .StoredProcedure, "Tiin_API$GetIdsFromSlugs", parameters);
        }

        public static string GenLinkImageFull(string Html)
        {
            try
            {
                Html = Html.Replace("/medias12/", Common.MediaRoot + "/medias12/");
                Html = Html.Replace("/media01/", Common.MediaRoot + "/media01/");
                Html = Html.Replace("/media02/", Common.MediaRoot + "/media02/");
                Html = Html.Replace("/media03/", Common.MediaRoot + "/media03/");
                Html = Html.Replace("/media04/", Common.MediaRoot + "/media04/");
                Html = Html.Replace("/media05/", Common.MediaRoot + "/media05/");
                Html = Html.Replace("/media06/", Common.MediaRoot + "/media06/");
                Html = Html.Replace("/media07/", Common.MediaRoot + "/media07/");
                Html = Html.Replace("/media08/", Common.MediaRoot + "/media08/");
                Html = Html.Replace("/media09/", Common.MediaRoot + "/media09/");
                Html = Html.Replace("/media10/", Common.MediaRoot + "/media10/");
                Html = Html.Replace("/media11/", Common.MediaRoot + "/media11/");
                Html = Html.Replace("../archive/images", "archive/images");
                Html = Html.Replace("/archive/images", "archive/images");
                Html = Html.Replace("archive/images", Common.MediaRootNew + "/archive/images");
                Html = Html.Replace("../archive/media/", "archive/media/");
                Html = Html.Replace("./archive/media/", "archive/media/");
                Html = Html.Replace("/archive/media/", "archive/media/");
                Html = Html.Replace("archive/media/", Common.MediaRootNew + "/archive/media/");
                Html = Html.Replace("../archive/audio/", "archive/audio/");
                Html = Html.Replace("/archive/audio/", "archive/audio/");
                Html = Html.Replace("./archive/audio/", "archive/audio/");
                Html = Html.Replace("archive/audio/", Common.MediaRootNew + "/archive/audio/");
                return Html;
            }
            catch (Exception)
            {
                return Html;
            }
        }

        #endregion

        #region "Log"

        /// <summary>
        /// 
        /// </summary>
        /// <param name="link"></param>
        /// <param name="name_control"></param>
        /// <param name="body"></param>
        /// <param name="other_info"></param>
        public static void LogException(string link, string name_control, string body, string other_info)
        {
            DataSet ds = new DataSet();
            try
            {
                SqlParameter[] parameters =
                {
                    new SqlParameter("@link", SqlDbType.NVarChar),
                    new SqlParameter("@name_control", SqlDbType.NVarChar),
                    new SqlParameter("@body", SqlDbType.NVarChar),
                    new SqlParameter("@other_info", SqlDbType.NVarChar),
                };
                parameters[0].Value = link;
                parameters[1].Value = name_control;
                parameters[2].Value = body;
                parameters[3].Value = other_info;
                ds = SqlHelper.ExecuteDataset(ConfigurationManager.AppSettings["cnString"],
                    CommandType.StoredProcedure, "Log_Exception$Insert", parameters);
            }
            catch (Exception)
            {
            }

            ;
        }

        #endregion

        #region "Load Control"

        public static void LoadBatchControl(Panel pnContainer, Page page, string pid)
        {
            DataSet dataSet = new DataSet();
            //dataSet = VatLid.DAL.ReadDataFull(int.Parse(pid), 0);
            try
            {
                int count_box = int.Parse(dataSet.Tables[0].Rows[0][0].ToString());
                int count = 0;
                for (int i = 0; i < count_box; i++)
                {
                    count++;
                    string box_name = dataSet.Tables[count].Rows[0]["BoxName"].ToString();
                    int post_count = int.Parse(dataSet.Tables[count].Rows[0]["PosCount"].ToString());
                    string url = "";
                    if (dataSet.Tables[count].Rows[0]["URL"] != null)
                    {
                        url = dataSet.Tables[count].Rows[0]["URL"].ToString();
                    }

                    for (int j = 0; j < post_count; j++)
                    {
                        count++;
                        int type = int.Parse(dataSet.Tables[count].Rows[0]["Type"].ToString());
                        int persistent = int.Parse(dataSet.Tables[count].Rows[0]["Persistent"].ToString());
                        if (persistent == 0)
                            url = "";
                        count++;
                        if (dataSet.Tables[count].Rows.Count > 0)
                        {
                            switch (type)
                            {
                                case 11:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/TitleList.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                case 12:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/Small_Image_Tittle_Left.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                case 13:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/Big_Image_Tittle_List.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                case 18:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/Big_Image_Small_Image_Tittle_Left.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                case 19:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/Medium_Image_Title_Under.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                case 30:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/Big_Image_Small_Image_Tittle_Under.ascx",
                                        box_name, url,
                                        dataSet.Tables[count]));
                                    break;
                                default:
                                    pnContainer.Controls.Add(LoadControl(page,
                                        "NewControls/TitleList.ascx",
                                        box_name,
                                        dataSet.Tables[count]));
                                    break;
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            ;
        }

        public static System.Web.UI.UserControl LoadControl(Page page, string UserControlPath,
            params object[] constructorParameters)
        {
            List<Type> constParamTypes = new List<Type>();
            foreach (object constParam in constructorParameters)
            {
                constParamTypes.Add(constParam.GetType());
            }

            System.Web.UI.UserControl ctl = page.LoadControl(UserControlPath) as System.Web.UI.UserControl;

            // Find the relevant constructor
            System.Reflection.ConstructorInfo constructor =
                ctl.GetType().BaseType.GetConstructor(constParamTypes.ToArray());

            //And then call the relevant constructor
            if (constructor == null)
            {
                throw new MemberAccessException("The requested constructor was not found on : " +
                                                ctl.GetType().BaseType.ToString());
            }
            else
            {
                constructor.Invoke(ctl, constructorParameters);
            }

            // Finally return the fully initialized UC
            return ctl;
        }

        #endregion

        public static string GetMSISDNOnHeader()
        {
            string useridHeader = "";
            try
            {
                StringBuilder header = new StringBuilder();
                foreach (string key in HttpContext.Current.Request.Headers.Keys)
                {
                    if (key.ToLower() == "msisdn")
                    {
                        useridHeader = HttpContext.Current.Request.Headers[key].ToString();
                    }
                }
            }
            catch
            {
            }

            return useridHeader;
        }

        public static string getPhoneNumber(HttpRequest rq)
        {
            string phone_number = "0";
            string ip = rq.UserHostAddress;
            if (rq.UserHostAddress.Trim().StartsWith("171.") ||
                rq.UserHostAddress.Trim().StartsWith("27.") ||
                rq.UserHostAddress.Trim().StartsWith("100.") ||
                rq.UserHostAddress.Trim().StartsWith("10."))
            {
                phone_number = getPhoneNumber(ip); //Get MSISDN from VAAA
            }
            else
            {
                return "0";
            }

            return phone_number;
        }

        public static string getPhoneNumber(string ip)
        {
            try
            {
                if ((HttpContext.Current.Session["MSISDN"] != null) &&
                    (HttpContext.Current.Session["MSISDN"].ToString().Length > 0))
                {
                    return HttpContext.Current.Session["MSISDN"].ToString();
                }

                if (isViettel_IP_PoolRange(ip))
                {
                    getMSISDN info = new getMSISDN();
                    info.username = "tinngan"; // ConfigurationManager.AppSettings["UserRadius"]; //"tinngan";
                    info.password =
                        "shortnews20120425"; // ConfigurationManager.AppSettings["PassRadius"]; //"shortnews20120425";
                    info.ip = ip;
                    Radius rd = new Radius();
                    rd.Timeout = 9000;
                    getMSISDNResponse rp = rd.getMSISDN(info);
                    if (rp.@return.code == 0)
                    {
                        HttpContext.Current.Session["MSISDN"] = rp.@return.desc;
                        return rp.@return.desc;
                    }
                    else
                    {
                        return rp.@return.code.ToString();
                    }
                }
                else
                {
                    return "0";
                }
            }
            catch (Exception)
            {
                return "00";
            }
        }

        public static bool isViettel_IP_PoolRange(string IP)
        {
            IPAddress incomingIp = IPAddress.Parse(IP);
            foreach (var subnet in Viettel_IP_PoolRange)
            {
                IPNetwork network = IPNetwork.Parse(subnet);
                if (IPNetwork.Contains(network, incomingIp))
                    return true;
            }

            return false;
        }

        private static List<string> Viettel_IP_PoolRange = new List<string>()
        {
            "10.1.0.0/16",
            "10.2.0.0/16",
            "10.3.0.0/16",
            "10.4.0.0/16",
            "10.5.0.0/16",
            "10.6.0.0/16",
            "10.7.0.0/16",
            "10.8.0.0/16",
            "10.9.0.0/16",
            "10.10.0.0/16",
            "10.11.0.0/16",
            "10.12.0.0/16",
            "10.13.0.0/16",
            "10.14.0.0/16",
            "10.15.0.0/16",
            "10.16.0.0/16",
            "10.17.0.0/16",
            "10.18.0.0/16",
            "10.19.0.0/16",
            "10.20.0.0/16",
            "10.21.0.0/16",
            "10.22.0.0/16",
            "10.23.0.0/16",
            "10.24.0.0/16",
            "10.25.0.0/16",
            "10.26.0.0/16",
            "10.27.0.0/16",
            "10.28.0.0/16",
            "10.29.0.0/16",
            "10.30.0.0/16",
            "10.31.0.0/16",
            "10.45.0.0/16",
            "10.46.0.0/16",
            "10.47.0.0/16",
            "10.62.0.0/16",
            "10.63.0.0/16",
            "10.76.0.0/16",
            "10.77.0.0/16",
            "10.80.0.0/16",
            "10.81.0.0/16",
            "10.82.0.0/16",
            "10.83.0.0/16",
            "10.84.0.0/16",
            "10.85.0.0/16",
            "10.86.0.0/16",
            "10.87.0.0/16",
            "10.88.0.0/16",
            "10.89.0.0/16",
            "10.90.0.0/16",
            "10.91.0.0/16",
            "10.92.0.0/16",
            "10.93.0.0/16",
            "10.94.0.0/16",
            "10.95.0.0/16",
            "10.96.0.0/16",
            "10.97.0.0/16",
            "10.98.0.0/16",
            "10.99.0.0/16",
            "10.100.0.0/16",
            "10.101.0.0/16",
            "10.102.0.0/16",
            "10.103.0.0/16",
            "10.104.0.0/16",
            "10.105.0.0/16",
            "10.106.0.0/16",
            "10.107.0.0/16",
            "10.108.0.0/16",
            "10.109.0.0/16",
            "10.110.0.0/16",
            "10.111.0.0/16",
            "10.112.0.0/16",
            "10.113.0.0/16",
            "10.114.0.0/16",
            "10.115.0.0/16",
            "10.116.0.0/16",
            "10.117.0.0/16",
            "10.118.0.0/16",
            "10.119.0.0/16",
            "10.122.0.0/16",
            "10.123.0.0/16",
            "10.125.0.0/16",
            "10.126.0.0/16",
            "10.127.0.0/16",
            "10.128.0.0/16",
            "10.129.0.0/16",
            "10.130.0.0/16",
            "10.131.0.0/16",
            "10.132.0.0/16",
            "10.133.0.0/16",
            "10.134.0.0/16",
            "10.135.0.0/16",
            "10.136.0.0/16",
            "10.137.0.0/16",
            "10.138.0.0/16",
            "10.139.0.0/16",
            "10.140.0.0/16",
            "10.141.0.0/16",
            "10.142.0.0/16",
            "10.143.0.0/16",
            "10.144.0.0/16",
            "10.145.0.0/16",
            "10.146.0.0/16",
            "10.147.0.0/16",
            "10.148.0.0/16",
            "10.149.0.0/16",
            "10.150.0.0/16",
            "10.151.0.0/16",
            "10.152.0.0/16",
            "10.153.0.0/16",
            "10.154.0.0/16",
            "10.155.0.0/16",
            "10.156.0.0/16",
            "10.157.0.0/16",
            "10.158.0.0/16",
            "10.159.0.0/16",
            "10.160.0.0/16",
            "10.161.0.0/16",
            "10.162.0.0/16",
            "10.163.0.0/16",
            "10.164.0.0/16",
            "10.165.0.0/16",
            "10.166.0.0/16",
            "10.167.0.0/16",
            "10.168.0.0/16",
            "10.169.0.0/16",
            "10.170.0.0/16",
            "10.171.0.0/16",
            "10.172.0.0/16",
            "10.173.0.0/16",
            "10.174.0.0/16",
            "10.175.0.0/16",
            "10.176.0.0/16",
            "10.177.0.0/16",
            "10.178.0.0/16",
            "10.179.0.0/16",
            "10.180.0.0/16",
            "10.181.0.0/16",
            "10.182.0.0/16",
            "10.183.0.0/16",
            "10.184.0.0/16",
            "10.185.0.0/16",
            "10.186.0.0/16",
            "10.187.0.0/16",
            "10.188.0.0/16",
            "10.189.0.0/16",
            "10.190.0.0/16",
            "10.191.0.0/16",
            "10.193.0.0/16",
            "10.194.0.0/16",
            "10.195.0.0/16",
            "10.197.0.0/16",
            "10.198.0.0/16",
            "10.199.0.0/16",
            "10.200.0.0/16",
            "10.206.0.0/16",
            "10.207.0.0/16",
            "10.208.0.0/16",
            "10.209.0.0/16",
            "10.210.0.0/16",
            "10.211.0.0/16",
            "10.220.0.0/16",
            "10.221.0.0/16",
            "10.222.0.0/16",
            "10.223.0.0/16",
            "10.224.0.0/16",
            "10.227.0.0/16",
            "10.232.0.0/16",
            "10.233.0.0/16",
            "10.234.0.0/16",
            "10.235.0.0/16",
            "10.236.0.0/16",
            "10.237.0.0/16",
            "10.238.0.0/16",
            "10.239.0.0/16",
            "10.243.0.0/16",
            "10.244.0.0/16",
            "10.245.0.0/16",
            "10.246.0.0/16",
            "100.64.0.0/16",
            "100.65.0.0/16",
            "100.66.0.0/16",
            "100.67.0.0/16",
            "100.68.0.0/16",
            "100.69.0.0/16",
            "100.70.0.0/16",
            "100.71.0.0/16",
            "100.72.0.0/16",
            "100.73.0.0/16",
            "100.74.0.0/16",
            "100.75.0.0/16",
            "100.76.0.0/16",
            "100.77.0.0/16",
            "100.78.0.0/16",
            "100.79.0.0/16",
            "100.80.0.0/16",
            "100.81.0.0/16",
            "100.82.0.0/16",
            "100.83.0.0/16",
            "100.84.0.0/16",
            "100.85.0.0/16",
            "100.86.0.0/16",
            "100.87.0.0/16",
            "100.88.0.0/16",
            "100.89.0.0/16",
            "100.90.0.0/16",
            "100.91.0.0/16",
            "100.92.0.0/16",
            "100.93.0.0/16",
            "100.94.0.0/16",
            "100.95.0.0/16",
            "100.96.0.0/16",
            "100.97.0.0/16",
            "100.98.0.0/16",
            "100.99.0.0/16",
            "100.100.0.0/16",
            "100.101.0.0/16",
            "100.102.0.0/16",
            "100.103.0.0/16",
            "100.104.0.0/16",
            "100.105.0.0/16",
            "100.106.0.0/16",
            "100.107.0.0/16",
            "100.108.0.0/16",
            "100.109.0.0/16",
            "100.110.0.0/16",
            "100.111.0.0/16",
            "100.112.0.0/16",
            "100.113.0.0/16",
            "100.114.0.0/16",
            "100.115.0.0/16",
            "100.116.0.0/16",
            "100.117.0.0/16",
            "100.118.0.0/16",
            "100.119.0.0/16",
            "100.120.0.0/16",
            "100.121.0.0/16",
            "100.122.0.0/16",
            "100.123.0.0/16",
            "100.124.0.0/16",
            "100.125.0.0/16",
            "100.126.0.0/16",
            "100.127.0.0/16",
            "27.65.0.0/16",
            "27.69.0.0/16",
            "27.70.0.0/16",
            "27.71.0.0/16",
            "27.73.0.0/16",
            "27.79.0.0/16",
            "171.227.0.0/16",
            "171.236.0.0/16",
            "171.237.0.0/16",
            "171.238.0.0/16",
            "171.239.0.0/16",
            "171.240.0.0/16",
            "171.241.0.0/16",
            "171.242.0.0/16",
            "171.243.0.0/16",
            "171.245.0.0/16",
            "171.252.0.0/16"
        };

        public static void LogADV(string id, string pid, string sTitle, string MSISDN, string UserAgent,
            string RawUrl, string ip)
        {
            try
            {
                if (ConfigurationManager.AppSettings["isLog"] == "1")
                {
                    QueueNameRabbitMQ queueName = new QueueNameRabbitMQ();

                    string contentlog = "";
                    contentlog = id + "|" + sTitle + "|" + MSISDN + "|" +
                                 UserAgent + "|http://m.tiin.vn/" +
                                 RawUrl + "|" + DateTime.Now.ToString("yyyyMMddHHmmss") +
                                 "|" + ip +
                                 "|vip";
                    string result = queueName.Call(ConfigurationManager.AppSettings["LogUserTiin"].ToString(),
                        contentlog);
                    queueName.Close();
                }
            }
            catch (Exception exception)
            {
                throw new LoggingException("RabbitMQ Log Error", exception);
            }
        }

        public static string AppendIdToUrl(string content)
        {
            var strRegexs = new Dictionary<int, string>
            {
                {1, @".+tiin\.vn/{1,2}chuyen-muc\/(?<category>[\w\-_]*)\/(?<slug>[\w\-_ \u00A0]+).html"},
                {
                    2,
                    @".+tiin\.vn/{1,2}tin-anh/chuyen-muc/(?<category>[\w\-_]*)\/(?<slug>[\w\-_ \u00A0]+)/(?<id>[\d]+).html"
                },
                {3, @".+tiin\.vn/{1,2}video/(?<category>.*)/(?<slug>[\w\-_ \u00A0]+)/(?<id>.*?).html"},
                {
                    4,
                    @".+tiin\.vn/{1,2}tin-media2/chuyen-muc/(?<category>[\w\-_]*)\/(?<slug>[\w\-_ \u00A0]+)/(?<id>[\d]+).html"
                },
                {5, @".+tiin\.vn/{1,2}chuyen-muc/(?<category>[\w\-_]*)/chuyen-de/(?<slug>.*?).html"},
                {6, @".+tiin\.vn/{1,2}chuyen-muc/sao/tin-sao/(?<slug>.*?).html"},
            };

            var aHrefRegex = new Regex(@"<a.*?href=[""'](?<url>.*?)[""'].*?>(?<name>.*?)</a>",
                RegexOptions.IgnoreCase);
            Match aMatch;
            var slugifies = new List<Slugify>();
            try
            {
                for (aMatch = aHrefRegex.Match(content); aMatch.Success; aMatch = aMatch.NextMatch())
                {
                    foreach (var strRegex in strRegexs)
                    {
                        var URL = aMatch.Groups["url"].Value;
                        var linkComp = Regex.Match(URL, strRegex.Value,
                            RegexOptions.IgnorePatternWhitespace | RegexOptions.Singleline);
                        if (linkComp.Success)
                        {
                            slugifies.Add(new Slugify
                            {
                                Type = strRegex.Key,
                                URL = URL,
                                Slug = linkComp.Groups["slug"] != null ? linkComp.Groups["slug"].Value : null,
                                Category = linkComp.Groups["category"] != null
                                    ? linkComp.Groups["category"].Value
                                    : null,
                                Id = linkComp.Groups["id"] != null ? linkComp.Groups["id"].Value : null,
                            });
                        }
                    }
                }

                var slugs =
                    string.Join("|",
                        slugifies.GroupBy(x => x.Type, x => x.Slug,
                            (key, g) => key + "@" + string.Join(",", g.ToArray())).ToArray());

                if (!string.IsNullOrEmpty(slugs))
                {
                    var dataSet = Common.GetIdsFromSlugs(slugs);


                    foreach (DataRow row in dataSet.Tables[0].Rows)
                    {
                        var item = slugifies.First(_ => _.Slug == (string) row["slug"]);
                        item.Id = row["id"].ToString();
                        item.Title = row["title"].ToString();
                    }

                    if (slugifies.Count != dataSet.Tables[0].Rows.Count)
                    {
                        AddToLogFile("WARNING: SLUG DONT MATCH\n" + JsonConvert.SerializeObject(slugifies) +
                                     "\nENDWARNING\n");
                    }

                    foreach (var slugify in slugifies.Where(_ => !string.IsNullOrEmpty(_.Id)))
                    {
                        content = content.Replace(
                            slugify.URL,
                            slugify.URL + (slugify.URL.Contains("?") ? "&" : "?") + "type=" +
                            slugify.Type + "&id=" + slugify.Id
                        );
                    }
                }
            }
            catch (Exception ex)
            {
                AddToLogFile("ERROR:\n" + JsonConvert.SerializeObject(slugifies) + "\nENDERROR\n");
            }

            return content;
        }

        public static void AddToLogFile(string content)
        {
            string fn = DateTime.Now.ToString("yyyy") + DateTime.Now.ToString("MM") +
                        DateTime.Now.ToString("dd") + DateTime.Now.ToString("HH") + ".txt";
            try
            {
                String path = "";
                path = ConfigurationManager.AppSettings["LOG_Radius"];
                path = path + "/" + DateTime.Now.ToString("yyyy") + "/" + DateTime.Now.ToString("MM") + "/" +
                       DateTime.Now.ToString("dd");
                if (!System.IO.File.Exists(path))
                {
                    System.IO.Directory.CreateDirectory(path);
                }

                path += "/" + fn;
                if (path != "")
                {
                    System.IO.StreamWriter writer = new StreamWriter(path, true, System.Text.Encoding.UTF8);
                    writer.WriteLine(content);
                    writer.Flush();
                    writer.Close();
                }
            }
            catch (Exception)
            {
                // ExceptionProcess(ex);
            }
        }
    }
}