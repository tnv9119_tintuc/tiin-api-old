using System;

namespace ViettelMedia.TiinApi.Commons
{
      public class LoggingException : Exception
      {
            public LoggingException(string message, Exception innerException) : base(message, innerException)
            {
            }
      }
}