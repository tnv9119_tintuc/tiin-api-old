﻿using System;
using System.Configuration;
using System.Text;
using RabbitMQ.Client;

namespace ViettelMedia.TiinApi.Commons
{
      // ReSharper disable once InconsistentNaming
      public class QueueNameRabbitMQ
      {
            //
            static string rbHostName = System.Configuration.ConfigurationManager.AppSettings["HostNameRMQ"].ToString();

            static int rbPort =
                  Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PortRMQ"].ToString());

            static string rbUser = System.Configuration.ConfigurationManager.AppSettings["UserNameRMQ"].ToString();

            static string rbPass = System.Configuration.ConfigurationManager.AppSettings["PasswordRMQ"].ToString();


            private ushort _heartbeat = 120;
            private int    _timeout   = 10;


            private IConnection           connection;
            private IModel                channel;
            private string                replyQueueName;
            private QueueingBasicConsumer consumer;

            public QueueNameRabbitMQ()
            {
                  ushort.TryParse(ConfigurationManager.AppSettings.Get("RMQHeartbeat"), out _heartbeat);
                  int.TryParse(ConfigurationManager.AppSettings.Get("RMQTimeout"), out _timeout);


                  var factory = new ConnectionFactory()
                                {
                                      HostName = rbHostName, Port = rbPort, UserName = rbUser, Password = rbPass,
                                };
                  factory.RequestedConnectionTimeout = _timeout;
                  factory.RequestedHeartbeat         = _heartbeat;

                  connection = factory.CreateConnection();

                  channel        = connection.CreateModel();
                  replyQueueName = channel.QueueDeclare().QueueName;
                  consumer       = new QueueingBasicConsumer(channel);
                  channel.BasicConsume(replyQueueName, true, consumer);
            }

            public String Call(string queueName, string message)
            {
                  try
                  {
                        var corrId = Guid.NewGuid().ToString();
                        var props  = channel.CreateBasicProperties();
                        props.ReplyTo       = replyQueueName;
                        props.CorrelationId = corrId;
                        var messageBytes = Encoding.UTF8.GetBytes(message);
                        //channel.BasicPublish("", "rpc_queue", props, messageBytes);
                        channel.BasicPublish("", queueName, props, messageBytes);
                        //var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                        return "1";
                  }
                  catch (Exception)
                  {
                        return "0";
                  }
            }

            public void Close()
            {
                  connection.Close();
            }
      }
}