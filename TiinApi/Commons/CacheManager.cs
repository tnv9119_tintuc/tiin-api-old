using System;
using System.Configuration;
using Memcached.ClientLibrary;

namespace ViettelMedia.TiinApi.Commons
{
      public static class CacheManager
      {
            public static MemcachedClient GetClient()
            {
                  var        serverList = ConfigurationManager.AppSettings.GetValues("Memcached");
                  SockIOPool pool       = SockIOPool.GetInstance();
                  pool.SetServers(serverList);

                  pool.InitConnections = 10;
                  pool.MinConnections  = 5;
                  pool.MaxConnections  = 100;

                  pool.SocketConnectTimeout = 600;
                  pool.SocketTimeout        = 800;

                  pool.MaintenanceSleep = 30;
                  pool.Failover         = true;
                  pool.Nagle            = false;
                  pool.Initialize();

                  return new MemcachedClient {EnableCompression = false};
            }

            public static T Remember<T>(string keyCache, DateTime expiredAt, Func<T> func)
            {
                  keyCache = Common.VERSION_PREFIX + "_" + keyCache;
                  var client = GetClient();
                  if (client.KeyExists(keyCache))
                  {
                        return (T) client.Get(keyCache);
                  }
                  var data = func.Invoke();
                  client.Set(keyCache, data, expiredAt);
                  return data;
            }
      }
}