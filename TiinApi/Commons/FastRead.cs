using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Net;
using System.Text.RegularExpressions;
using HtmlAgilityPack;
using ViettelMedia.TiinApi.Models;

namespace ViettelMedia.TiinApi.Commons
{
    public class FastRead
    {
        private readonly string _urlMedia = ConfigurationManager.AppSettings["MediaRootNew"];
        private readonly string _urlMediaDownload = ConfigurationManager.AppSettings["MediaRootDownload"];

        public List<ArticleBlock> GetContentBlocks(string rawContent, string postId, out int total, int count = 0)
        {
            var keyCache = Common.VERSION_PREFIX + "_" + "ExtractContentBlocks:" + ":" +
                           StringUtils.Md5Hash(rawContent);
            var client = CacheManager.GetClient();
            var data = new List<ArticleBlock>();
            if (client.KeyExists(keyCache))
            {
                total = 0;
                data = (List<ArticleBlock>) client.Get(keyCache);
            }
            else
            {
                data = ExtractContentBlocks(rawContent, postId, out total, count);
                client.Set(keyCache, data, DateTime.Now.AddMinutes(Common.MinutesCache));
            }

            return (count > 0 && data.Count > count) ? data.GetRange(0, count) : data;
        }

        public List<ArticleBlock> ExtractContentBlocks(string rawContent, string postId, out int total, int count = 0)
        {
            var listResult = new List<ArticleBlock>();

            var errorDownload = false;
            rawContent = HtmlEntity.DeEntitize(rawContent);
            //Thay thế div chú thích ảnh bằng thẻ p (Đồng bộ với nội dung) 
            rawContent = Regex.Replace(rawContent, "<(?:div|p) class=[\"']p-chuthich[\"'][^>]*>(.*?)</(?:div|p)>",
                "<p class=\"isNote\">$1</p>");
            // Loại bỏ các thẻ không cần thiết.
            var doc = new HtmlDocument();
            doc.LoadHtml(rawContent);

            total = doc.DocumentNode.ChildNodes.Count;
            int blockId = 0;

            if (doc.DocumentNode.ChildNodes.Count > 0)
            {
                doc.DocumentNode.RemoveChildExcept(
                    "p", "div", "br", "video", "source", "img", "a", "strong", "em",
                    "table", "thead", "tbody", "tr", "td", "ul", "ol", "li"
                );
            }

            foreach (var node in doc.DocumentNode.ChildNodes)
            {
                if (node.HasClass("multi-item"))
                {
                    try
                    {
                        foreach (var child in node.Clone().ChildNodes)
                        {
                            node.ParentNode.InsertBefore(child, node);
                        }
                        node.RemoveAll();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine(e);
                    }
                    
                }
               
            }


            foreach (var node in doc.DocumentNode.ChildNodes)
            {
                
                var block = new ArticleBlock();
                var blockContent = node.OuterHtml;
                string image;
                block.Type = 1;

                #region Detect Video

                if (blockContent.IndexOf("<video", StringComparison.Ordinal) != -1)
                {
                    string poster = Regex.Match(blockContent,
                        @"(poster)\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?").Value;
                    if (blockContent.ToLower().Contains(".jpg") && (!poster.ToLower().Contains(".jpg")))
                        poster = Regex.Match(blockContent, "<img.+?src=[\"'](.+?)[\"'].*?>",
                            RegexOptions.IgnoreCase).Groups[1].Value;
                    if (blockContent.ToLower().Contains(".png") && (!poster.ToLower().Contains(".png")))
                        poster = Regex.Match(blockContent, "<img.+?src=[\"'](.+?)[\"'].*?>",
                            RegexOptions.IgnoreCase).Groups[1].Value;
                    image = poster.Replace("poster=\"", "").Replace("\"", "");

                    var src = Regex.Match(blockContent, @"(src)\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?")
                        .Value;
                    var video = src.Replace("src=\"", "").Replace("\"", "");
                    block.Image = image;
                    var wSize = 0;
                    var hSize = 0;
                    try
                    {
                        if (!Regex.IsMatch(image, "^(http|https)://"))
                        {
                            image = UrlHelper.NormalizeUrl(_urlMedia.RemoveLastSlash() + "/" + image);
                        }

                        using (var client = new WebClient())
                        {
                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["proxy"]))
                            {
                                client.Proxy =
                                    new WebProxy(ConfigurationManager.AppSettings["proxy"]);
                            }

                            var downloadUrl = image.Replace(_urlMedia.RemoveLastSlash(),
                                _urlMediaDownload.RemoveLastSlash());
                            using (var stream = client.OpenRead(downloadUrl))
                            {
                                if (stream != null)
                                {
                                    using (var img = Image.FromStream(stream))
                                    {
                                        wSize = img.Width;
                                        hSize = img.Height;
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
//                        image += "#Video Error..." + ex.Message;
                        errorDownload = true;
                    }

                    if (video.Length > 10)
                    {
                        video = video.Replace(".http://", "http://");

                        block.Type = 3;
                        block.Image = image.ToLower();
                        block.Video = video.ToLower();
                        block.Width = wSize;
                        block.Height = hSize;
                    }
                }

                #endregion

                #region Detect Photo

                if (blockContent.IndexOf("<img", StringComparison.Ordinal) != -1)
                {
                    string src = Regex.Match(blockContent, @"(src)\s*=\s*[""']?([^'"" >]+?)[ '""][^>]*?")
                        .Value;

                    if (blockContent.ToLower().Contains(".jpg") && (!src.ToLower().Contains(".jpg")))
                        src = Regex.Match(blockContent, "<img.+?src=[\"'](.+?)[\"'].*?>",
                            RegexOptions.IgnoreCase).Groups[1].Value;

                    if (blockContent.ToLower().Contains(".png") && (!src.ToLower().Contains(".png")))
                        src = Regex.Match(blockContent, "<img.+?src=[\"'](.+?)[\"'].*?>",
                            RegexOptions.IgnoreCase).Groups[1].Value;
                    image = src.Replace("src=\"", "").Replace("\"", "");
                    block.Image = image;
                    int wSize = 0;
                    int hSize = 0;
                    try
                    {
                        if (!Regex.IsMatch(image, "^(http|https)://"))
                        {
                            image = UrlHelper.NormalizeUrl(_urlMedia.RemoveLastSlash() + "/" + image);
                        }

                        var downloadUrl = image.Replace(_urlMedia.RemoveLastSlash(),
                            _urlMediaDownload.RemoveLastSlash());
                        var size = CacheManager.Remember("IMG_DIMENSION_" + downloadUrl,
                            DateTime.Now.AddDays(7), () =>
                            {
                                using (var client = new WebClient())
                                {
                                    if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["proxy"]))
                                    {
                                        client.Proxy =
                                            new WebProxy(ConfigurationManager.AppSettings["proxy"]);
                                    }

                                    Console.WriteLine($@"Downloading: {downloadUrl}");
                                    using (var stream = client.OpenRead(downloadUrl))
                                    {
                                        if (stream == null) return new Size(640, 360);
                                        using (var img = Image.FromStream(stream))
                                        {
                                            return new Size(img.Width, img.Height);
                                        }
                                    }
                                }
                            });
                        hSize = size.Height;
                        wSize = size.Width;
                    }
                    catch (Exception ex)
                    {
//                        image += "#Image Error..." + ex.Message;
                        errorDownload = true;
                    }

                    if (image.Length > 10)
                    {
                        block.Type = 2;
                        block.Image = image.ToLower();
                        block.Width = wSize;
                        block.Height = hSize;
                    }
                }

                #endregion

                #region Detect Template Blocks

                if (node.HasClass("more-detail"))
                {
                    node.RemoveChildExcept("a", "img", "br");
                    block.Template = "read-more";
                }


                if (node.HasClass("box-tamsu"))
                {
                    node.RemoveChildExcept("a", "em", "br");
                    block.Template = "box-info";
                }

               

                #endregion


                #region Detect Content

                if (!string.IsNullOrEmpty(node.InnerText) && !string.IsNullOrWhiteSpace(node.InnerText))
                {
                    var txtContent = node.InnerHtml;
                    if (node.HasAttributes && node.Attributes["class"] != null &&
                        node.Attributes["class"].Value.Contains("isNote"))
                    {
                        block.Template = "caption";
                    }

                    if (txtContent.Length > 1)
                    {
                        block.Content = txtContent;
                        block.Length = node.InnerText.Length;
                    }
                }

                #endregion

                if ( block.Content.Length > 0 || block.Image.Length > 0 || block.Video.Length > 0)
                {
                    listResult.Add(block);    
                }
                if (blockId >= count && count > 0)
                {
                    break;
                }
            }

            if (errorDownload)
            {
                //lỗi download ảnh
                //                              CommonRadius.AddToLogFile(string.Format("{0:yyyyMMddHHmm}: lỗi download ảnh \n" + id, DateTime.Now));
            }

            return listResult;
        }
    }
}