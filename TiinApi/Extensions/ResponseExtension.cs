using System.Web;
using Newtonsoft.Json;
using TiinApi;
using ViettelMedia.TiinApi.Models.Response;

namespace ViettelMedia.TiinApi.Extensions
{
    public static class ResponseExtension
    {
        public static void Json(this HttpResponse response, object data, int statusCode)
        {
            var setting = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Include
            };
            response.Clear();
            response.ContentType = "application/json; charset=utf-8";
            response.StatusCode = statusCode;
            response.Write(JsonConvert.SerializeObject(data, setting));
            response.End();
        }

        public static void Json(this HttpResponse response, DataResponse data)
        {
            Json(response, data, 200);
        }

        public static void JsonString(this HttpResponse response, string jsonString)
        {
            response.Clear();
            response.ContentType = "application/json; charset=utf-8";
            response.StatusCode = 200;
            response.Write(jsonString);
            response.End();
        }
    }
}