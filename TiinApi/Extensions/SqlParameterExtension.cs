using System.Data.SqlClient;

namespace ViettelMedia.TiinApi.Extensions
{
      public static class SqlParameterExtension
      {
            public static SqlParameter SetValue(this SqlParameter parameter, object value)
            {
                  parameter.Value = value;
                  return parameter;
            }
      }
}