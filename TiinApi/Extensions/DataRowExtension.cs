using System.Data;

namespace ViettelMedia.TiinApi.Extensions
{
      public static class DataRowExtension
      {
            public static bool HasColumn(this DataRow dataRow, string columnName)
            {
                  return dataRow.Table.Columns.Contains(columnName);
            }

            public static string StringValue(this DataRow dataRow, string columnName)
            {
                  return dataRow.HasColumn(columnName)
                               ? dataRow[columnName].ToString()
                               : null;
            }

            public static bool IsNullOrEmptyStringValue(this DataRow dataRow, string columnName)
            {
                  return string.IsNullOrEmpty(dataRow.StringValue(columnName));
            }
      }
}