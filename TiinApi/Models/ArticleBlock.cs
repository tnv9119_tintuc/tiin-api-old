using System;
using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
    [Serializable]
    public class ArticleBlock
    {
        [JsonProperty("type")] public int Type { get; set; }

        [JsonIgnore] public string Html { get; set; }

        [JsonProperty("content")] public string Content { get; set; }

        [JsonProperty("length")] public int Length { get; set; }

        [JsonProperty("video")] public string Video { get; set; }

        [JsonProperty("image")] public string Image { get; set; }

        [JsonProperty("width")] public int Width { get; set; }

        [JsonProperty("height")] public int Height { get; set; }

        [JsonProperty("paragraph")] public string Template { get; set; }

        public ArticleBlock()
        {
            Type = 1;
            Html = "";
            Content = "";
            Length = 0;
            Video = "";
            Image = "";
            Width = 0;
            Height = 0;
            Template = null;
        }
    }
}