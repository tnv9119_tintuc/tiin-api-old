﻿using System.Collections.Generic;
using TiinApi;

namespace ViettelMedia.TiinApi.Models.Response
{
      public class DataProfileOfStar : DataResponse
      {
            public DataProfileOfStar(List<ProfileOfStar> data)
            {
                  this.data = data;
            }
      }
}