using System;
using System.Collections;
using System.Linq;
using Newtonsoft.Json;
using ViettelMedia.TiinApi.Commons;

namespace ViettelMedia.TiinApi.Models.Response
{
      public class DataResponse
      {
            public object data { get; set; }

//            [JsonIgnore]
            public Exception exception { private get; set; }

            public bool error
            {
                  get { return exception != null && !(exception is LoggingException); }
            }

//            [JsonIgnore]
            public string message { get; set; }

//            [JsonIgnore]
            public string waring
            {
                  get
                  {
                        return exception != null ? exception.Message +
                               "(" +
                               exception.StackTrace + ")" : null;

//                        return exception is LoggingException
//                                     ? exception.Message                                                          +
//                                       "("                                                                        +
//                                       exception.StackTrace.Split(new[] {" in "}, StringSplitOptions.None).Last() + ")"
//                                     : null;
                  }
            }

            public string ToJson()
            {
                  return JsonConvert.SerializeObject(this);
            }
      }
}