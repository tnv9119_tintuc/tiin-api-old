﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using Newtonsoft.Json.Linq;
using vtwap.vt.ptnd.Radius;

namespace TiinWeb.Solr
{
    public class ParamsObject
    {
        public string Q { get; set; }
        public string Df { get; set; }
        public int Start { get; set; }
        public int Rows { get; set; }
    }

    public class ResponseHeader
    {
        public int Status { get; set; }
        public int QTime { get; set; }
        public ParamsObject Params { get; set; }
    }

    public class Message
    {
        public static readonly string WEB_ROOT = ConfigurationManager.AppSettings["WebRoot"].ToString();
        public static readonly string MEDIA_ROOT_NEW = ConfigurationManager.AppSettings["MediaRootNew"].ToString();
        public static readonly string MEDIA_ROOT = ConfigurationManager.AppSettings["MediaRoot"].ToString();

        public static readonly int MAX_MEDIA_OLD_ID = ConfigurationManager.AppSettings["Max_id_img_old"] != null
            ? int.Parse(ConfigurationManager.AppSettings["Max_id_img_old"])
            : 0;

        public string Id { get; set; }
        public int Cid { get; set; }
        public int Pid { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
        public string Lead { get; set; }
        public string LeadImage { get; set; }
        public DateTime DatePub { get; set; }
        public string CategoryName { get; set; }
        public string CategoryAlias { get; set; }
        public int Icon { get; set; }
        public int isTinMedia { get; set; }
        public int Status { get; set; }

        public string Content { get; set; }

        public string URLDetail()
        {
            string link = "";

            try
            {
                if (Icon == 0 || Icon == 1) //Tin bài thông thường, ảnh slide
                {
                    link = WEB_ROOT + String.Format("/chuyen-muc/{0}/{1}.html", CategoryAlias, Slug);
                }
                else if (Icon == 3) //Ảnh
                {
                    link = WEB_ROOT + String.Format("/tin-anh/chuyen-muc/{0}/{1}/{2}.html", CategoryAlias, Slug, Id);
                }
                else if (Icon == 2) //Video
                {
                    link = WEB_ROOT + String.Format("/video/{0}/{1}/{2}.html", CategoryAlias, Slug, Id);
                }
                else if (Icon == 5) //Tin mutilmedia
                {
                    link = WEB_ROOT + String.Format("/tin-media/chuyen-muc/{0}/{1}/{2}.html", CategoryAlias, Slug, Id);
                }
                else if (Icon == 6) //mutimedia
                {
                    link = WEB_ROOT + String.Format("/tin-media2/chuyen-muc/{0}/{1}/{2}.html", CategoryAlias, Slug, Id);
                }
                else if (Icon == 7) //mutimedia
                {
                    link = WEB_ROOT + String.Format("/tin-media3/chuyen-muc/{0}/{1}/{2}.html", CategoryAlias, Slug, Id);
                }
                else
                {
                    link = WEB_ROOT + String.Format("/chuyen-muc/{0}/{1}.html", CategoryAlias, Slug);
                }
            }
            catch
            {
                link = WEB_ROOT + String.Format("/chuyen-muc/{0}/{1}.html", CategoryAlias, Slug);
            }

            return link;
        }

        public string URLImage()
        {
            string link_image = "";

            try
            {
                if (int.Parse(Id) >= MAX_MEDIA_OLD_ID)
                    link_image = MEDIA_ROOT_NEW + "/" + LeadImage;
                else
                    link_image = MEDIA_ROOT + LeadImage;
            }
            catch
            {
            }

            return link_image;
        }
    }

    public class Response
    {
        public int NumFound { get; set; }
        public int Start { get; set; }
        public List<Message> Docs { get; set; }
    }


    public class SolrResponse
    {
        public ResponseHeader ResponseHeader { get; set; }
        public Response Response { get; set; }
        public JObject Highlighting { get; set; }
    }
}