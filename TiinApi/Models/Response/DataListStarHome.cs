﻿using System.Collections.Generic;
using TiinApi;

namespace ViettelMedia.TiinApi.Models.Response
{
    public class DataListStarHome : DataResponse
    {
          public DataListStarHome(List<ListStarHome> data)
         {
             this.data = data;
         }

    }
}
