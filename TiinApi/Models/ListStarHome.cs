﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace ViettelMedia.TiinApi.Models
{
      public class ListStarHome
      {
            [JsonProperty("type")]
            public int Type { get; set; }

            [JsonProperty("name")]
            public string Name { get; set; }

            [JsonProperty("list_star")]
            public List<ListStar> ListStar { get; set; }


            public ListStarHome()
            {
                  Type     = 0;
                  Name     = "";
                  ListStar = new List<ListStar>();
            }
      }
}