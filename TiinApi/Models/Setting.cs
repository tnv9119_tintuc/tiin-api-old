﻿using Newtonsoft.Json;

// ReSharper disable StringLiteralTypo

namespace ViettelMedia.TiinApi.Models
{
      public class Setting
      {
            [JsonProperty("android_version")]
            public int AndroidVersion { get; set; }

            [JsonProperty("ios_version")]
            public int IosVersion { get; set; }

            [JsonProperty("advertise")]
            public int Advertise { get; set; }

            [JsonProperty("admobbaner")]
            public string AdmobBanner { get; set; }

            [JsonProperty("admobfullscreen")]
            public string AdmobFullScreen { get; set; }

            [JsonProperty("advertise_ios")]
            public int AdvertiseIos { get; set; }

            [JsonProperty("admobnaner_ios")]
            public string AdmobBannerIos { get; set; }

            [JsonProperty("admobfullscreen_ios")]
            public string AdmobFullScreenIos { get; set; }

            [JsonProperty("time_delay_ads")]
            public int TimeDelayAds { get; set; }

            public Setting()
            {
                  AndroidVersion     = 0;
                  IosVersion         = 0;
                  Advertise          = 0;
                  AdmobBanner        = "";
                  AdmobFullScreen    = "";
                  AdvertiseIos       = 0;
                  AdmobBannerIos     = "";
                  AdmobFullScreenIos = "";
                  TimeDelayAds       = 0;
            }
      }
}