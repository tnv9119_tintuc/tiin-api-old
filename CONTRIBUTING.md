# CONTRIBUTING

### MASTER

- Phiên bản hiện tại đang chạy (Sử dụng Memcached)

### DEVELOP

- Phiên bản đã quy hoạch và chuyển đổi sang cache Redis (Đã test nhưng chưa triển khai)


### NHÁNH NATIVE-APP

- Phiên bản cũ, dùng cho App Native (App riêng của Tiin trên Appstore và Playstore - Không phải phiên bản trên Mocha)
